/*
 var store_subcategoria = new Ext.data.Store({
 reader: new Ext.data.JsonReader({
 fields: ['id_subcategoria', 'nombre_subcategoria'],
 root: 'data'
 }),
 proxy: new Ext.data.HttpProxy({
 url: 'index.php/accion/cargar_subcategoria',
 method: 'POST'
 })
 });
 */
var store_combo_doctor = new Ext.data.Store({
    reader: new Ext.data.JsonReader({
        fields: ['id', 'nombres'],
        root: 'data'
    }),
    proxy: new Ext.data.HttpProxy({
        url: 'index.php/doctor/cargar_doctores',
        method: 'POST'
    })
});

var store_combo_estado = new Ext.data.Store({
    reader: new Ext.data.JsonReader({
        fields: ['id', 'nombre'],
        root: 'data'
    }),
    proxy: new Ext.data.HttpProxy({
        url: 'index.php/registro_trabajo/cargar_estados',
        method: 'POST'
    })
});
var store_combo_cambiar_estado = new Ext.data.Store({
    reader: new Ext.data.JsonReader({
        fields: ['id', 'nombre'],
        root: 'data'
    }),
    proxy: new Ext.data.HttpProxy({
        url: 'index.php/registro_trabajo/cargar_estados',
        method: 'POST'
    }),
    listeners: {
        load: function () {
            store_combo_cambiar_estado.remove(store_combo_cambiar_estado.getById(1));
            if (estado == 1) {
                store_combo_cambiar_estado.remove(store_combo_cambiar_estado.getById(3));
            }
            if(estado == 4){
                store_combo_cambiar_estado.remove(store_combo_cambiar_estado.getById(2));
                store_combo_cambiar_estado.remove(store_combo_cambiar_estado.getById(3));
                store_combo_cambiar_estado.remove(store_combo_cambiar_estado.getById(4));
            }
            store_combo_cambiar_estado.remove(store_combo_cambiar_estado.getById(estado));
        }
    }
});
//----------------------------Stores Grid--------------------------------

var store_registro_trabajo = new Ext.data.JsonStore({
    url: 'index.php/registro_trabajo/get_registros_trabajos',
    root: 'data',
    fields: ['id','observaciones', 'nombre_estado', 'id_estado', 'fecha_entrada', 'paciente', 'nombre_doctor', 'clinica', 'fecha_entrega', 'hora_entrega', 'orden', 'trabajador', 'observaciones']
});
var store_doctores = new Ext.data.JsonStore({
    url: 'index.php/doctor/get_doctores',
    root: 'data',
    fields: ['id', 'nombres', 'apellidos', { name: 'ruc', type: 'int' }, { name: 'celular', type: 'int' },
        { name: 'telefono', type: 'int' }, { name: 'correo', type: 'string' }, { name: 'direccion', type: 'string'}]
});

var store_usuarios = new Ext.data.JsonStore({
    url: 'index.php/accion/buscar_usuarios',
    root: 'data',
    fields: ['id_usuario', 'usuario', 'nombre', 'apellidos', 'email', 'telefono', 'activo']
});

var store_estados_registro_trabajo = new Ext.data.JsonStore({
    url: 'index.php/registro_trabajo/get_estados_registro_trabajo',
    root: 'data',
    fields: ['id', 'nombre_estado', 'fecha_entrada', 'observaciones', 'fecha_entrega', 'hora_entrega', 'factura']
});

