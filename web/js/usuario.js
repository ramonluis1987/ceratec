function Mostrar_Usuario() {
    var mostrar_insertar_usuario = new Ext.Window({
        title: 'Insertar Usuario',
        id: 'mostrar_insertar_usuario',
        bodyStyle: 'padding:15px;background:transparent',
        border: true,
        layout: 'form',

        modal: true,
        autoHeight: true,
        width: '450',

        closeAction: 'close',
        resizable: false,
        items: [insertar_usuario_form],
        buttons: [
            {
                text: 'Insertar',
                handler: function () {
                    Insertar_Usuario_Submit();
                }
            }
        ]
    });
    mostrar_insertar_usuario.show();
}

var insertar_usuario_form = {
    xtype: 'form',
    id: 'insertar_usuario_form',
    bodyStyle: 'padding:15px;background:transparent',
    border: false,
    defaultType: 'textfield',
    defaults: {
        msgTarget: 'side',
        labelStyle: 'color:#1f4981;font-weight:bold;font-size:11px;'
    },
    items: [
        {
            allowBlank: false,
            fieldLabel: 'Usuario',		//vtype: 'digitos',
            id: 'text_usuario_insertar_usuario',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Usuario_Submit();
                    }
                }
            }
        },
        {
            allowBlank: false,
            fieldLabel: 'Nombre',		//vtype: 'digitos',
            id: 'text_nombre_insertar_usuario',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Usuario_Submit();
                    }
                }
            }
        },
        {
            allowBlank: false,
            fieldLabel: 'Apellidos',		//vtype: 'digitos',
            id: 'text_apellidos_insertar_usuario',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Usuario_Submit();
                    }
                }
            }
        },
        {
            allowBlank: false,
            fieldLabel: 'Email',		//vtype: 'digitos',
            vtype: 'email',
            id: 'text_email_insertar_usuario',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Usuario_Submit();
                    }
                }
            }
        },
        {
            allowBlank: false,
            fieldLabel: 'Tel&eacute;fono',		//vtype: 'digitos',
            id: 'text_telefono_insertar_usuario',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Usuario_Submit();
                    }
                }
            }
        },
        {
            allowBlank: false,
            fieldLabel: 'Password',
            //vtype: 'digitos',
            inputType: 'password',
            id: 'text_password_insertar_usuario',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Usuario_Submit();
                    }
                }
            }
        },
        {
            allowBlank: false,
            xtype: 'combo',
            id: 'combo_activo_insertar_usuario',
            fieldLabel: 'Activo',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            anchor: '95%',
            mode: 'local',
            store: new Ext.data.ArrayStore({
                id: 1,
                fields: [
                    'id',
                    'data'
                ],
                data: [
                    [1, 'Activo'],
                    [0, 'Inactivo']
                ]
            }),
            valueField: 'id',
            displayField: 'data'
        }
    ]
};
function Insertar_Usuario_Submit() {
    var usuario = Ext.getCmp('text_usuario_insertar_usuario').getValue();
    var nombre = Ext.getCmp('text_nombre_insertar_usuario').getValue();
    var apellidos = Ext.getCmp('text_apellidos_insertar_usuario').getValue();
    var email = Ext.getCmp('text_email_insertar_usuario').getValue();
    var telefono = Ext.getCmp('text_telefono_insertar_usuario').getValue();
    var activo = Ext.getCmp('combo_activo_insertar_usuario').getValue();
    var password = calcMD5(Ext.getCmp('text_password_insertar_usuario').getValue());
    if (true/*nombre_actividad != '' && id_puesto != 0*/) {


        Ext.Ajax.request({
            url: 'index.php/accion/insertar_usuario',
            waitMsg: 'Insertando Usuario ...',
            method: 'POST',
            params: {
                password: password,
                usuario: usuario,
                nombre: nombre,
                apellidos: apellidos,
                email: email,
                telefono: telefono,
                activo: activo
            },
            success: function (fp, o) {
                data = Ext.util.JSON.decode(fp.responseText);
                if(data.existe)
                {
                    Ext.Msg.show({
                        title: 'Informaci&oacute;n..!!',
                        msg: 'Ya existe el usario..!!!!',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO
                    });
                }
                else{
                    Ext.Msg.show({
                        title: 'Informaci&oacute;n..!!',
                        msg: 'Se ha insertado el Usuario Correctamente',
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.INFO,
                        fn: function (btn) {
                            if ((btn == "ok")) {
                                Ext.Msg.show({
                                    title: 'Acci&oacute;n?',
                                    msg: 'Desea insertar otro Usuario?',
                                    buttons: Ext.Msg.YESNO,
                                    fn: function (btn) {
                                        if (btn == "yes") {
                                            Ext.getCmp('insertar_usuario_form').getForm().reset();
                                        }
                                        else {
                                            Ext.getCmp('mostrar_insertar_usuario').close();
                                        }
                                    },
                                    icon: Ext.MessageBox.QUESTION
                                });
                            }
                        }
                    });
                }
            }
        });
    } else {
        Ext.Msg.show({
            title: 'Alerta',
            msg: 'Los campor con (*) son obligatorios',
            buttons: Ext.Msg.OK,

            icon: Ext.MessageBox.ERROR
        });
    }

}
//------------------------buscar usuario--------------------
function Mostrar_Buscar_Usuarios() {
    if (!Ext.getCmp('grid_buscar_usuarios')) {
        var grid_buscar_usuarios = new Ext.grid.GridPanel({
            id: 'grid_buscar_usuarios',
            layout: 'fit',
            //bodyStyle: 'padding:5px',
            height: '450',
            title: 'Listado de Usuarios',
            ds: store_usuarios,
            autoScroll: true,
            clicksToEdit: '1',
            columns: [
                {
                    header: "ID",
                    width: 35,
                    dataIndex: 'id_usuario',
                    sortable: true
                },
                {
                    header: "Usuario",
                    width: 80,
                    dataIndex: 'usuario',
                    sortable: true
                },
                {
                    header: "Nombre",
                    width: 200,
                    dataIndex: 'nombre',
                    sortable: true
                },
                {
                    header: "Apellidos",
                    width: 100,
                    dataIndex: 'apellidos',
                    sortable: true
                },
                {
                    header: "Email",
                    width: 80,
                    dataIndex: 'email',
                    sortable: true
                },
                {
                    header: "Tel&eacute;fono",
                    width: 80,
                    dataIndex: 'telefono',
                    sortable: true
                },
                {
                    header: "Activo",
                    width: 90,
                    dataIndex: 'activo',
                    sortable: true
                }
            ],
            selModel: new Ext.grid.RowSelectionModel({
                singleSelect: true
            })/*,
             bbar: new Ext.PagingToolbar({
             pageSize: 15,
             store: store_acciones,
             displayInfo: true,
             afterPageText: 'de {0}',
             beforePageText: 'P&aacute;gina',
             displayMsg: 'Acciones {0} - {1} de {2}',
             emptyMsg: 'No hay datos para mostrar',
             firstText: 'Primera p&aacute;gina',
             lastText: '&Uacute;ltima p&aacute;gina',
             nextText: 'P&aacute;gina siguente',
             prevText: 'P&aacute;gina anterior',
             refreshText: 'Actualizar'
             })*/,
            loadMask: true,
            tbar: [
                {
                    text: 'Modificar Usuario',
                    icon: window.App.base_url+'web/imagenes/star.png',
                    id: 'boton_modificar_Usuario',
                    disabled: false,
                    handler: function () {
                        var sm = grid_buscar_usuarios.getSelectionModel();
                        if (sm.getSelected()) {
                            id_usuario = sm.getSelected().get('id_usuario');
                            Mostrar_Modificar_Usuario();
                            Ext.getCmp('modificar_usuario_form').getForm().load({
                                url: 'index.php/accion/cargar_usuario_por_id',
                                params: {
                                    id_usuario: id_usuario
                                },
                                waitMsg: 'Cargando..!!',
                                success: function (form, action) {
                                    data = action.result.data;

                                    combo_activo = Ext.getCmp('combo_activo_modificar_usuario');
                                    combo_activo.setValue(data.activo);
                                    /*combo_activo = Ext.getCmp('combo_activo_modificar_usuario');
                                     combo_activo.getStore().load();
                                     combo_activo.getStore().on('load', function () {
                                     combo_activo.setValue(data.activo);
                                     }, this);*/
                                }
                            });
                        }
                        else {
                            Ext.Msg.alert('Alerta', 'Seleccione el Usuario a modificar');
                        }
                    }
                }
            ]
        })
        //Ext.getCmp('contenedor_buscar_acciones').add(grid_buscar_accion);
        //Ext.getCmp('contenedor_buscar_acciones').doLayout();
        var w_mostrar_buscar_usuarios = new Ext.Window({
            title: 'Buscar Usuarios',
            id: 'w_mostrar_buscar_Usuarios',
            bodyStyle: 'padding:15px;background:transparent',
            border: true,
            layout: 'form',
            modal: true,
            autoHeight: true,
            width: 1000,
            closeAction: 'close',
            resizable: true,
            items: [grid_buscar_usuarios]
        });
        w_mostrar_buscar_usuarios.show();
    }
}
function Mostrar_Modificar_Usuario() {

    var modificar_usuario_form = {
        xtype: 'form',
        id: 'modificar_usuario_form',
        bodyStyle: 'padding:15px;background:transparent',
        border: false,
        defaultType: 'textfield',
        defaults: {
            msgTarget: 'side',
            labelStyle: 'color:#1f4981;font-weight:bold;font-size:11px;'
        },
        items: [
            {
                xtype: 'hidden',
                id: 'id_usuario_modificar',
                name: 'id_usuario'
            },
            {
                allowBlank: false,
                fieldLabel: 'Usuario',		//vtype: 'digitos',
                id: 'text_usuario_modificar_usuario',
                anchor: '95%',
                name: 'usuario',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            modificar_Usuario_Submit();
                        }
                    }
                }
            },
            {
                allowBlank: false,
                fieldLabel: 'Nombre',		//vtype: 'digitos',
                id: 'text_nombre_modificar_usuario',
                anchor: '95%',
                name: 'nombre',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            modificar_Usuario_Submit();
                        }
                    }
                }
            },
            {
                allowBlank: false,
                fieldLabel: 'Apellidos',		//vtype: 'digitos',
                id: 'text_apellidos_modificar_usuario',
                anchor: '95%',
                name: 'apellidos',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            modificar_Usuario_Submit();
                        }
                    }
                }
            },
            {
                allowBlank: false,
                fieldLabel: 'Email',		//vtype: 'digitos',
                vtype: 'email',
                id: 'text_email_modificar_usuario',
                name: 'email',
                anchor: '95%',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            modificar_Usuario_Submit();
                        }
                    }
                }
            },
            {
                allowBlank: false,
                fieldLabel: 'Tel&eacute;fono',		//vtype: 'digitos',
                id: 'text_telefono_modificar_usuario',
                anchor: '95%',
                name: 'telefono',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            modificar_Usuario_Submit();
                        }
                    }
                }
            },
            {
                allowBlank: false,
                xtype: 'combo',
                id: 'combo_activo_modificar_usuario',
                name:'activo',
                hiddenName:'activo',
                fieldLabel: 'Activo',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                anchor: '95%',
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 1,
                    fields: [
                        'id',
                        'data'
                    ],
                    data: [
                        [1, 'Activo'],
                        [0, 'Inactivo']
                    ]
                }),
                valueField: 'id',
                displayField: 'data'
            }
        ]
    };
    var mostrar_modificar_usuario = new Ext.Window({
        title: 'Modificar Usuario',
        id: 'mostrar_modificar_usuario',
        bodyStyle: 'padding:15px;background:transparent',
        border: true,
        layout: 'form',
        modal: true,
        autoHeight: true,
        width: '450',
        closeAction: 'close',
        resizable: false,
        items: [modificar_usuario_form],
        buttons: [
            {
                text: 'Modificar',
                handler: function () {
                    Modificar_Usuario_Submit();
                }
            }
        ]
    });
    mostrar_modificar_usuario.show();
}
function Modificar_Usuario_Submit() {
    id_usuario = Ext.getCmp('id_usuario_modificar').getValue();
    Ext.getCmp('modificar_usuario_form').getForm().submit({
        url: 'index.php/accion/modificar_usuario',
        waitMsg: 'Modificando Usuario ...',
        success: function (fp, o) {
            store_usuarios.reload();
           // Ext.getCmp('grid_buscar_usuarios').getStore().reload();
            Ext.Msg.show({
                title: 'Informaci&oacute;n..!!',
                msg: 'Se ha modificado el Usuario Correctamente',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO,
                fn: function (btn) {
                    if (btn == "ok") {
                        Ext.getCmp('mostrar_modificar_usuario').close();
                    }
                }
            });
            //Ext.getCmp('mostrar_modificar_usuario').close();
        }
    });
}

var buscar_usuarios_panel = new Ext.FormPanel({
    id: 'buscar_productos_panel',
    layout: 'fit',
    labelAlign: 'top',
    frame: true,
    collapsible: true,
    title: 'Buscar Usuarios',
    bodyStyle: 'padding:5px',
    autoScroll: true,
    overflow: 'auto',
    //width: 400,
    height: 'auto',
    defaults: {
        msgTarget: 'side',
        anchor: '95%'
    },
    items: [
        {
            defaults: {
                msgTarget: 'side',
                anchor: '95%'
            },
            layout: 'column',
            items: [
                {
                    defaults: {
                        msgTarget: 'side',
                        anchor: '95%'
                    },
                    columnWidth: .5,
                    layout: 'form',
                    items: [
                        {
                            xtype: 'textfield',
                            id: 'text_usuario_buscar_usuario',
                            fieldLabel: 'Usuario',
                            name: 'usuario',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Usuario_Submit();
                                        Mostrar_Buscar_Usuarios();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            id: 'text_nombre_buscar_usuario',
                            fieldLabel: 'Nombre',
                            name: 'nombre',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Usuario_Submit();
                                        Mostrar_Buscar_Usuarios();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            id: 'text_apellidos_buscar_usuario',
                            fieldLabel: 'Apellidos',
                            name: 'apellidos',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Usuario_Submit();
                                        Mostrar_Buscar_Usuarios();
                                    }
                                }
                            }
                        }
                    ]
                },
                {
                    columnWidth: .5,
                    layout: 'form',
                    items: [
                        {
                            xtype: 'textfield',
                            id: 'text_email_buscar_usuario',
                            fieldLabel: 'Email',
                            name: 'email',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Usuario_Submit();
                                        Mostrar_Buscar_Usuarios();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            id: 'text_telefono_buscar_usuario',
                            fieldLabel: 'Tel&eacute;fono',
                            name: 'email',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Usuario_Submit();
                                        Mostrar_Buscar_Usuarios();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'combo',
                            id: 'combo_activo_buscar_usuario',
                            fieldLabel: 'Activo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            name: 'activo',
                            anchor: '95%',
                            mode: 'local',
                            store: new Ext.data.ArrayStore({
                                id: 1,
                                fields: [
                                    'id',
                                    'data'
                                ],
                                data: [
                                    [1, 'Activo'],
                                    [0, 'Inactivo']
                                ]
                            }),
                            valueField: 'id',
                            displayField: 'data',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Usuario_Submit();
                                        Mostrar_Buscar_Usuarios();
                                    }
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ],
    buttons: [
        {
            text: 'Buscar',
            handler: function () {
                Buscar_Usuario_Submit();
                Mostrar_Buscar_Usuarios();
            }
        }
    ]

});
function Buscar_Usuario_Submit() {
    store_usuarios.baseParams.usuario = Ext.getCmp('text_usuario_buscar_usuario').getValue();
    store_usuarios.baseParams.nombre = Ext.getCmp('text_nombre_buscar_usuario').getValue();
    store_usuarios.baseParams.apellidos = Ext.getCmp('text_apellidos_buscar_usuario').getValue();
    store_usuarios.baseParams.email = Ext.getCmp('text_email_buscar_usuario').getValue();
    store_usuarios.baseParams.telefono = Ext.getCmp('text_telefono_buscar_usuario').getValue();
    store_usuarios.baseParams.activo = Ext.getCmp('combo_activo_buscar_usuario').getValue();


    store_usuarios.load(/*{
     params: {
     start: 0,
     limit: 15
     }
     }*/);

}
