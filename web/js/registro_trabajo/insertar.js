function Mostrar_Insertar_Registro_Trabajo() {
    var insertar_registro_trabajo_form = {
        xtype: 'form',
        id: 'insertar_registro_trabajo_form',
        bodyStyle: 'padding:15px;background:transparent',
        border: false,
        defaultType: 'textfield',
        defaults: {
            msgTarget: 'side',
            labelStyle: 'color:#1f4981;font-weight:bold;font-size:11px;'
        },
        items: [
            {
                allowBlank: false,
                fieldLabel: 'Paciente',		//vtype: 'digitos',
                id: 'text_paciente_insertar_registro_trabajo',
                name:'paciente',
                anchor: '95%',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            Insertar_Registro_Trabajo_Submit();
                        }
                    }
                }
            },{
                xtype: 'combo',
                id: 'combo_doctor_insertar_registro_trabajo',
                fieldLabel: 'Doctor',
                mode: 'remote',
                allowBlank: false,
                store: store_combo_doctor,
                typeAhead: true,
                displayField: 'nombres',
                name:'id_doctor',
                valueField: 'id',
                hiddenName:'id_doctor',
                // width: 350,
                minChars:1,
                anchor: '95%',
                forceSelection: true,
                selectOnFocus: true,
                triggerAction: 'all'
            },
            {
                allowBlank: false,
                fieldLabel: 'Cl&iacute;nica',		//vtype: 'digitos',
                id: 'text_clinica_insertar_registro_trabajo',
                name:'clinica',
                anchor: '95%',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            Insertar_Registro_Trabajo_Submit();
                        }
                    }
                }
            },{
                id: 'text_fecha_entrega_registro_trabajo',
                xtype: 'datefield',
                format: 'Y-m-d',
                // width: 350,
                emptyText: 'Fecha Entrega',
                fieldLabel: 'Fecha Entrega',
                name: 'fecha_entrega',
                allowBlank: false,
                anchor: '95%',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            Insertar_Registro_Trabajo_Submit();
                        }
                    }
                }
            },{
                id: 'text_hora_entrega_registro_trabajo',
                xtype: 'timefield',
                //format: 'Y-m-d',
                // width: 350,
                fieldLabel: 'Hora Entrega',
                name: 'hora_entrega',
                allowBlank: false,
                anchor: '95%',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            Insertar_Registro_Trabajo_Submit();
                        }
                    }
                }
            },
            {
                allowBlank: false,
                fieldLabel: 'Orden',
                id: 'text_orden_insertar_registro_trabajo',
                name:'orden',
                anchor: '95%',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            Insertar_Registro_Trabajo_Submit();
                        }
                    }
                }
            },{
                allowBlank: false,
                fieldLabel: 'Trabajador',
                id: 'text_trabajador_insertar_registro_trabajo',
                name:'trabajador',
                anchor: '95%',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            Insertar_Registro_Trabajo_Submit();
                        }
                    }
                }
            },{
                allowBlank: false,
                fieldLabel: 'Observaciones',
                id: 'text_observaciones_insertar_registro_trabajo',
                name:'observaciones',
                anchor: '95%',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            Insertar_Registro_Trabajo_Submit();
                        }
                    }
                }
            }
        ]
    };

    var mostrar_insertar_registro_trabajo = new Ext.Window({
        title: 'Insertar Registro Trabajo',
        id: 'mostrar_insertar_registro_trabajo',
        bodyStyle: 'padding:15px;background:transparent',
        border: true,
        layout: 'form',

        modal: true,
        autoHeight: true,
        width: '450',
        resizable: false,

        closeAction: 'close',
        items: [insertar_registro_trabajo_form],
        buttons: [
            {
                text: 'Insertar',
                handler: function () {
                    Insertar_Registro_Trabajo_Submit();
                }
            }
        ]
});
    mostrar_insertar_registro_trabajo.show();
}



function Insertar_Registro_Trabajo_Submit(){
    Ext.getCmp('insertar_registro_trabajo_form').getForm().submit({
        url : 'index.php/registro_trabajo/insertar_registro_trabajo',
        waitMsg : 'Insertando Registro Trabajo ...',
        success : function(fp, o) {
            if (o.result.existe) {
                Ext.Msg.show({
                    title: 'Informaci&oacute;n..!!',
                    msg: 'Ya existe un registro_trabajo con el orden insertado..!!',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            }
            else{
                Ext.Msg.show({
                    title: 'Informaci&oacute;n..!!',
                    msg: 'Se ha Insertado el Registro Trabajo Correctamente',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    fn: function (btn) {
                        if ((btn == "ok")) {
                            Ext.Msg.show({
                                title: 'Acci&oacute;n?',
                                msg: 'Desea insertar otro registro_trabajo?',
                                buttons: Ext.Msg.YESNO,
                                fn: function (btn) {
                                    if (btn == "yes") {
                                        Ext.getCmp('insertar_registro_trabajo_form').getForm().reset();
                                    }
                                    else {
                                        Ext.getCmp('mostrar_insertar_registro_trabajo').close();
                                    }
                                },
                                icon: Ext.MessageBox.QUESTION
                            });
                        }
                    }
                });
            }

        }
    });
}

