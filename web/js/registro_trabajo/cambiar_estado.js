function mostrar_cambiar_estado_registro_trabajo(id_registro_trabajo) {
    var mostrar_cambiar_estado_registro_trabajo = new Ext.Window({
        title: 'Cambiar Estado Registro Trabajo',
        id: 'mostrar_cambiar_estado_registro_trabajo',
        bodyStyle: 'padding:15px;background:transparent',
        border: true,
        layout: 'form',

        modal: true,
        autoHeight: true,
        width: '450',
        resizable: false,

        closeAction: 'close',
        items: [cambiar_estado_registro_trabajo_form],
        buttons: [
            {
                text: 'Cambiar Estado',
                handler: function () {
                    cambiar_estado_registro_trabajo_submit(id_registro_trabajo);
                }
            }
        ]
    });
    mostrar_cambiar_estado_registro_trabajo.show();
}

var cambiar_estado_registro_trabajo_form = {
    xtype: 'form',
    id: 'cambiar_estado_registro_trabajo_form',
    bodyStyle: 'padding:15px;background:transparent',
    border: false,
    defaultType: 'textfield',
    defaults: {
        msgTarget: 'side',
        labelStyle: 'color:#1f4981;font-weight:bold;font-size:11px;'
    },
    items: [
        {
            allowBlank: false,
            fieldLabel: 'Observaciones',		//vtype: 'digitos',
            id: 'text_observaciones_cambiar_estado_registro_trabajo',
            name: 'observaciones',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        cambiar_estado_registro_trabajo_submit();
                    }
                }
            }
        },
        {
            xtype: 'combo',
            id: 'combo_estado_cambiar_estado_registro_trabajo',
            fieldLabel: 'Estado',
            mode: 'remote',
            store: store_combo_cambiar_estado,
            typeAhead: true,
            displayField: 'nombre',
            valueField: 'id',
            hiddenName: 'id',
            // width: 350,
            minChars: 1,
            anchor: '95%',
            forceSelection: true,
            selectOnFocus: true,
            triggerAction: 'all',
            listeners: {
                select: function () {
                    if (Ext.getCmp('combo_estado_cambiar_estado_registro_trabajo').getValue() == 3) {
                        Ext.getCmp('text_fecha_entrega_cambiar_estado_registro_trabajo').enable();
                        Ext.getCmp('text_hora_entrega_cambiar_estado_registro_trabajo').enable();
                        Ext.getCmp('text_factura_cambiar_estado_registro_trabajo').disable();
                    }
                    if (Ext.getCmp('combo_estado_cambiar_estado_registro_trabajo').getValue() == 5) {
                        Ext.getCmp('text_fecha_entrega_cambiar_estado_registro_trabajo').disable();
                        Ext.getCmp('text_hora_entrega_cambiar_estado_registro_trabajo').disable();
                        Ext.getCmp('text_factura_cambiar_estado_registro_trabajo').enable();
                    }
                    if (Ext.getCmp('combo_estado_cambiar_estado_registro_trabajo').getValue() == 4) {
                        Ext.getCmp('text_fecha_entrega_cambiar_estado_registro_trabajo').disable();
                        Ext.getCmp('text_hora_entrega_cambiar_estado_registro_trabajo').disable();
                        Ext.getCmp('text_factura_cambiar_estado_registro_trabajo').disable();
                    }
                }
            }
        },
        {
            id: 'text_fecha_entrega_cambiar_estado_registro_trabajo',
            xtype: 'datefield',
            format: 'Y-m-d',
            // width: 350,
            emptyText: 'Fecha Entrega',
            fieldLabel: 'Fecha Entrega',
            name: 'fecha_entrega',
            allowBlank: false,
            anchor: '95%',
            disabled: true,
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        cambiar_estado_registro_trabajo_submit();
                    }
                }
            }
        },
        {
            id: 'text_hora_entrega_cambiar_estado_registro_trabajo',
            xtype: 'timefield',
            //format: 'Y-m-d',
            // width: 350,
            fieldLabel: 'Hora Entrega',
            name: 'hora_entrega',
            allowBlank: false,
            anchor: '95%',
            disabled: true,
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        cambiar_estado_registro_trabajo_submit();
                    }
                }
            }
        },
        {
            allowBlank: false,
            fieldLabel: 'Factura',		//vtype: 'digitos',
            id: 'text_factura_cambiar_estado_registro_trabajo',
            name: 'factura',
            disabled: true,
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        cambiar_estado_registro_trabajo_submit();
                    }
                }
            }
        }

    ]
};

function cambiar_estado_registro_trabajo_submit(id_registro_trabajo) {
    Ext.getCmp('cambiar_estado_registro_trabajo_form').getForm().submit({
        url: 'index.php/registro_trabajo/cambiar_estado',
        params: {
            id_registro_trabajo: id_registro_trabajo
        },
        waitMsg: 'Cambiando Registro Trabajo ...',
        success: function (fp, o) {
            Ext.Msg.show({
                title: 'Informaci&oacute;n..!!',
                msg: 'Se ha Cambiado el Estado del Registro Trabajo Correctamente',
                buttons: Ext.Msg.OK,
                icon: Ext.MessageBox.INFO
            });
            store_registro_trabajo.reload();
            Ext.getCmp('mostrar_cambiar_estado_registro_trabajo').close();
        }
    });
}


