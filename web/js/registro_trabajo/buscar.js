var estado = 0;
function mostrar_grid_buscar_registro_rrabajo() {
    if (!Ext.getCmp('grid_buscar_registro_trabajo')) {
        var grid_buscar_registro_trabajo = new Ext.grid.GridPanel({
            id: 'grid_buscar_registro_trabajo',
            layout: 'fit',
            //bodyStyle: 'padding:5px',
            height: '450',
            title: 'Listado de Registro Trabajo',
            ds: store_registro_trabajo,
            autoScroll: true,
            clicksToEdit: '1',
            columns: [
                {
                    header: "ID",
                    width: 35,
                    dataIndex: 'id',
                    sortable: true
                },
                {
                    header: "Observaciones",
                    width: 120,
                    dataIndex: 'observaciones',
                    sortable: true
                },
                {
                    header: "Estado",
                    width: 120,
                    dataIndex: 'nombre_estado',
                    sortable: true
                },
                {
                    header: "ID Estado",
                    hidden: true,
                    width: 120,
                    dataIndex: 'id_estado',
                    sortable: true
                },
                {
                    header: "Fecha Entrada",
                    width: 120,
                    dataIndex: 'fecha_entrada',
                    sortable: true
                },
                {
                    header: "Paciente",
                    width: 120,
                    dataIndex: 'paciente',
                    sortable: true
                },
                {
                    header: "Doctor",
                    width: 100,
                    dataIndex: 'nombre_doctor',
                    sortable: true
                },
                {
                    header: "Cl&iacute;nica",
                    width: 80,
                    dataIndex: 'clinica',
                    sortable: true
                },
                {
                    header: "Fecha Entrega",
                    width: 80,
                    dataIndex: 'fecha_entrega',
                    sortable: true
                },
                {
                    header: "Hora Entrega",
                    width: 100,
                    dataIndex: 'hora_entrega',
                    sortable: true
                },
                {
                    header: "Orden",
                    width: 200,
                    dataIndex: 'orden',
                    sortable: true
                },
                {
                    header: "Trabajador",
                    width: 200,
                    dataIndex: 'trabajador',
                    sortable: true
                }
            ],
            selModel: new Ext.grid.RowSelectionModel({
                singleSelect: true
            })/*,
             bbar: new Ext.PagingToolbar({
             pageSize: 15,
             store: store_acciones,
             displayInfo: true,
             afterPageText: 'de {0}',
             beforePageText: 'P&aacute;gina',
             displayMsg: 'Acciones {0} - {1} de {2}',
             emptyMsg: 'No hay datos para mostrar',
             firstText: 'Primera p&aacute;gina',
             lastText: '&Uacute;ltima p&aacute;gina',
             nextText: 'P&aacute;gina siguente',
             prevText: 'P&aacute;gina anterior',
             refreshText: 'Actualizar'
             })*/,
            loadMask: true,
            tbar: [
                /*{
                 text: 'Modificar Registro Trabajo',
                 icon: '../pedidos/web/imagenes/star.png',
                 id: 'boton_modificar_registro_trabajo',
                 disabled: false,
                 handler: function () {
                 var sm = grid_buscar_registro_trabajo.getSelectionModel();
                 if (sm.getSelected()) {
                 id_registro_trabajo = sm.getSelected().get('id');
                 Mostrar_Modificar_registro_trabajo();
                 Ext.getCmp('modificar_registro_trabajo_form').getForm().load({
                 url: 'index.php/registro_trabajo/get_registro_trabajo_por_id',
                 params: {
                 id_registro_trabajo: id_registro_trabajo
                 },
                 waitMsg: 'Cargando..!!',
                 success: function (form, action) {
                 data = action.result.data;
                 }
                 });
                 }
                 else {
                 Ext.Msg.alert('Alerta', 'Seleccione el Registro de Trabajo a Modificar');
                 }
                 }
                 },*/
                {
                    text: 'Ver Estado De Registro de Trabajo',
                    icon: window.App.base_url+'web/imagenes/star.png',
                    id: 'boton_estado_registro_trabajo',
                    disabled: false,
                    handler: function () {
                        var sm = grid_buscar_registro_trabajo.getSelectionModel();
                        if (sm.getSelected()) {
                            id_registro_trabajo = sm.getSelected().get('id');
                            mostrar_grid_estados_registro_trabajo();
                            ver_estados_registro_trabajo_submit(id_registro_trabajo);
                        }
                        else {
                            Ext.Msg.alert('Alerta', 'Seleccione el Registro de Trabajo');
                        }
                    }
                },
                {
                    text: 'Cambiar Estado',
                    icon: window.App.base_url+'web/imagenes/star.png',
                    id: 'boton_cambiar_estado_registro_trabajo',
                    disabled: false,
                    handler: function () {
                        var sm = grid_buscar_registro_trabajo.getSelectionModel();
                        if (sm.getSelected()) {
                            id_registro_trabajo = sm.getSelected().get('id');
                            id_estado = sm.getSelected().get('id_estado');
                            estado = id_estado;
                            if (estado != 5) {
                                mostrar_cambiar_estado_registro_trabajo(id_registro_trabajo, id_estado);
                            }
                            else{
                                Ext.Msg.alert('Alerta', 'El Trabajo ya esta pagado. No se puede cambiar de estado...!!!');
                            }
                        }
                        else {
                            Ext.Msg.alert('Alerta', 'Seleccione el Registro de Trabajo');
                        }
                    }
                }
            ]
        })
        //Ext.getCmp('contenedor_buscar_acciones').add(grid_buscar_accion);
        //Ext.getCmp('contenedor_buscar_acciones').doLayout();
        var w_mostrar_buscar_registro_trabajo = new Ext.Window({
            title: 'Buscar Registro Trabajo',
            id: 'w_mostrar_buscar_registro_trabajo',
            bodyStyle: 'padding:15px;background:transparent',
            border: true,
            layout: 'form',
            modal: true,
            autoHeight: true,
            width: 1000,
            closeAction: 'close',
            resizable: true,
            items: [grid_buscar_registro_trabajo]
        });
        w_mostrar_buscar_registro_trabajo.show();
    }
}
var buscar_registro_trabajo_panel = new Ext.FormPanel({
    title: 'Buscar Registro de Trabajo',
    layout: 'table',
    defaults: {
        // applied to each contained panel
        bodyStyle: 'padding:20px'
    },
    layoutConfig: {
        // The total column count must be specified here
        columns: 3
    },
    items: [
        {
            id: 'buscar_registro_trabajo_panel',
            layout: 'fit',
            labelAlign: 'top',
            frame: false,
            //collapsible: true,
            //title: 'Buscar Registro Trabajo',
            bodyStyle: 'padding:5px',
            autoScroll: true,
            overflow: 'auto',
            width: 650,
            height: 300,
            border: false,
            bodyBorder: false,
            hideBorders: true,
            rowspan: 5,
            colspan: 5,
            defaults: {
                msgTarget: 'side',
                anchor: '95%'
            },
            items: [
                {
                    defaults: {
                        msgTarget: 'side',
                        anchor: '95%',
                        border: false,
                        bodyBorder: false,
                        hideBorders: true
                    },
                    layout: 'column',
                    items: [
                        {
                            defaults: {
                                msgTarget: 'side',
                                anchor: '95%', border: false,
                                bodyBorder: false,
                                hideBorders: true
                            },
                            columnWidth: .5,
                            layout: 'form',
                            items: [
                                {
                                    id: 'text_fecha_entrega_desde_buscar_registro_trabajo',
                                    xtype: 'datefield',
                                    format: 'Y-m-d',
                                    // width: 350,
                                    emptyText: 'Fecha Entrega Desde',
                                    fieldLabel: 'Fecha Entrega Desde',
                                    name: 'fecha_entrega_desde',
                                    anchor: '95%',
                                    listeners: {
                                        specialkey: function (field, e) {
                                            if (e.getKey() == Ext.EventObject.ENTER) {
                                                buscar_registro_trabajo_Submit();
                                                bostrar_grid_buscar_registro_trabajo();

                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    id: 'text_paciente_buscar_registro_trabajo',
                                    fieldLabel: 'Paciente',
                                    name: 'paciente',
                                    anchor: '95%',
                                    listeners: {
                                        specialkey: function (field, e) {
                                            if (e.getKey() == Ext.EventObject.ENTER) {
                                                buscar_registro_trabajo_Submit();
                                                bostrar_grid_buscar_registro_trabajo();
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'combo',
                                    id: 'combo_doctor_buscar_registro_trabajo',
                                    fieldLabel: 'Doctor',
                                    mode: 'remote',
                                    store: store_combo_doctor,
                                    typeAhead: true,
                                    displayField: 'nombres',
                                    name: 'id_doctor',
                                    valueField: 'id',
                                    hiddenName: 'id',
                                    // width: 350,
                                    minChars: 1,
                                    anchor: '95%',
                                    forceSelection: true,
                                    selectOnFocus: true,
                                    triggerAction: 'all'
                                },
                                {
                                    xtype: 'textfield',
                                    id: 'text_clinica_buscar_registro_trabajo',
                                    fieldLabel: 'Cl&iacute;nica',
                                    name: 'clinica',
                                    anchor: '95%',
                                    listeners: {
                                        specialkey: function (field, e) {
                                            if (e.getKey() == Ext.EventObject.ENTER) {
                                                buscar_registro_trabajo_submit();
                                                mostrar_grid_Buscar_registro_trabajo();
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    id: 'text_observaciones_buscar_registro_trabajo',
                                    fieldLabel: 'Observaciones',
                                    name: 'observaciones',
                                    anchor: '95%',
                                    listeners: {
                                        specialkey: function (field, e) {
                                            if (e.getKey() == Ext.EventObject.ENTER) {
                                                buscar_registro_trabajo_submit();
                                                mostrar_grid_Buscar_registro_trabajo();
                                            }
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            columnWidth: .5,
                            layout: 'form',
                            items: [
                                {
                                    id: 'text_fecha_entrega_hasta_buscar_registro_trabajo',
                                    xtype: 'datefield',
                                    format: 'Y-m-d',
                                    // width: 350,
                                    emptyText: 'Fecha Entrega Hasta',
                                    fieldLabel: 'Fecha Entrega Hasta',
                                    name: 'fecha_entrega_hasta',
                                    anchor: '95%',
                                    listeners: {
                                        specialkey: function (field, e) {
                                            if (e.getKey() == Ext.EventObject.ENTER) {
                                                buscar_registro_trabajo_Submit();
                                                bostrar_grid_buscar_registro_trabajo();
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    id: 'text_trabajador_buscar_registro_trabajo',
                                    fieldLabel: 'Trabajador',
                                    name: 'trabajador',
                                    anchor: '95%',
                                    listeners: {
                                        specialkey: function (field, e) {
                                            if (e.getKey() == Ext.EventObject.ENTER) {
                                                buscar_registro_trabajo_submit();
                                                mostrar_grid_buscar_registro_rrabajo();
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    id: 'text_orden_buscar_registro_trabajo',
                                    fieldLabel: 'Orden',
                                    name: 'orden',
                                    anchor: '95%',
                                    listeners: {
                                        specialkey: function (field, e) {
                                            if (e.getKey() == Ext.EventObject.ENTER) {
                                                buscar_registro_trabajo_submit();
                                                mostrar_grid_buscar_registro_trabajo();
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'combo',
                                    id: 'combo_estado_buscar_registro_trabajo',
                                    fieldLabel: 'Estado',
                                    mode: 'remote',
                                    store: store_combo_estado,
                                    typeAhead: true,
                                    displayField: 'nombre',
                                    valueField: 'id',
                                    hiddenName: 'id',
                                    // width: 350,
                                    minChars: 1,
                                    anchor: '95%',
                                    forceSelection: true,
                                    selectOnFocus: true,
                                    triggerAction: 'all'
                                }
                            ]
                        }
                    ]
                }
            ],

            buttons: [
                {
                    text: 'Buscar',
                    handler: function () {
                        buscar_registro_trabajo_submit();
                        mostrar_grid_buscar_registro_rrabajo();
                    }
                }
            ]

        }/*,
         {
         html: '<p>Cell B content</p>',
         colspan: 2
         },
         {
         html: '<p>Cell C content</p>',
         cellCls: 'highlight'
         },
         {
         html: '<p>Cell D content</p>'
         }*/
    ]
});

function buscar_registro_trabajo_submit() {
    store_registro_trabajo.baseParams.fecha_entrega_desde = Ext.getCmp('text_fecha_entrega_desde_buscar_registro_trabajo').getRawValue();
    store_registro_trabajo.baseParams.fecha_entrega_hasta = Ext.getCmp('text_fecha_entrega_hasta_buscar_registro_trabajo').getRawValue();
    store_registro_trabajo.baseParams.paciente = Ext.getCmp('text_paciente_buscar_registro_trabajo').getValue();
    store_registro_trabajo.baseParams.id_doctor = Ext.getCmp('combo_doctor_buscar_registro_trabajo').getValue();
    store_registro_trabajo.baseParams.clinica = Ext.getCmp('text_clinica_buscar_registro_trabajo').getValue();
    store_registro_trabajo.baseParams.orden = Ext.getCmp('text_orden_buscar_registro_trabajo').getValue();
    store_registro_trabajo.baseParams.trabajador = Ext.getCmp('text_trabajador_buscar_registro_trabajo').getValue();
    store_registro_trabajo.baseParams.id_estado = Ext.getCmp('combo_estado_buscar_registro_trabajo').getValue();
    store_registro_trabajo.baseParams.observaciones = Ext.getCmp('text_observaciones_buscar_registro_trabajo').getValue();


    store_registro_trabajo.load(/*{
     params: {
     start: 0,
     limit: 15
     }
     }*/);

}

var contenedor_buscar_registro_trabajo = {
    id: 'contenedor_buscar_registro_trabajo',
    //layout: 'fit',
    border: false,
    autoScroll: true,
    bodyStyle: 'padding:5px',
    items: [buscar_registro_trabajo_panel]
}

function mostrar_buscar_registro_trabajo() {
    panel_buscar_registro_trabajo = Ext.getCmp('contentPanel-id');
    if (!Ext.getCmp('contenedor_buscar_registro_trabajo')) {
        panel_buscar_registro_trabajo.add(contenedor_buscar_registro_trabajo);
        Ext.getCmp('contentPanel-id').layout.setActiveItem('contenedor_buscar_registro_trabajo');
    }
    else {
        Ext.getCmp('contentPanel-id').layout.setActiveItem('contenedor_buscar_registro_trabajo');
    }
}

