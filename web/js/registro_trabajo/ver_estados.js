function mostrar_grid_estados_registro_trabajo() {
    if (!Ext.getCmp('grid_estados_registro_trabajo')) {
        var grid_estados_registro_trabajo = new Ext.grid.GridPanel({
            id: 'grid_estados_registro_trabajo',
            layout: 'fit',
            //bodyStyle: 'padding:5px',
            height: '450',
            title: 'Estados Registro Trabajo',
            ds: store_estados_registro_trabajo,
            autoScroll: true,
            clicksToEdit: '1',
            columns: [
                {
                    header: "ID",
                    width: 35,
                    dataIndex: 'id',
                    sortable: true
                },
                {
                    header: "Estado",
                    width: 120,
                    dataIndex: 'nombre_estado',
                    sortable: true
                },
                {
                    header: "Fecha Entrada",
                    width: 120,
                    dataIndex: 'fecha_entrada',
                    sortable: true
                },
                {
                    header: "Observaciones",
                    width: 120,
                    dataIndex: 'observaciones',
                    sortable: true
                },
                {
                    header: "Fecha Entrega",
                    width: 120,
                    dataIndex: 'fecha_entrega',
                    sortable: true
                },
                {
                    header: "Hora Entrega",
                    width: 120,
                    dataIndex: 'hora_entrega',
                    sortable: true
                },
                {
                    header: "Factura",
                    width: 100,
                    dataIndex: 'factura',
                    sortable: true
                }
            ],
            selModel: new Ext.grid.RowSelectionModel({
                singleSelect: true
            })/*,
             bbar: new Ext.PagingToolbar({
             pageSize: 15,
             store: store_acciones,
             displayInfo: true,
             afterPageText: 'de {0}',
             beforePageText: 'P&aacute;gina',
             displayMsg: 'Acciones {0} - {1} de {2}',
             emptyMsg: 'No hay datos para mostrar',
             firstText: 'Primera p&aacute;gina',
             lastText: '&Uacute;ltima p&aacute;gina',
             nextText: 'P&aacute;gina siguente',
             prevText: 'P&aacute;gina anterior',
             refreshText: 'Actualizar'
             })*/,
            loadMask: true,
            tbar: [
            ]
        })
        //Ext.getCmp('contenedor_buscar_acciones').add(grid_buscar_accion);
        //Ext.getCmp('contenedor_buscar_acciones').doLayout();
        var w_mostrar_ver_estados_registro_trabajo = new Ext.Window({
            title: 'Estados Registro Trabajo',
            id: 'w_mostrar_ver_estados_registro_trabajo',
            bodyStyle: 'padding:15px;background:transparent',
            border: true,
            layout: 'form',
            modal: true,
            autoHeight: true,
            width: 1000,
            closeAction: 'close',
            resizable: true,
            items: [grid_estados_registro_trabajo]
        });
        w_mostrar_ver_estados_registro_trabajo.show();
    }
}

function ver_estados_registro_trabajo_submit(id_registro_trabajo) {
    store_estados_registro_trabajo.baseParams.id_registro_trabajo = id_registro_trabajo;


    store_estados_registro_trabajo.load(/*{
     params: {
     start: 0,
     limit: 15
     }
     }*/);

}
