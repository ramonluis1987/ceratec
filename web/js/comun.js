var decimal = /^\d*\.?\d{1,2}$/;
var flotante = /^([0-9])*[.]?[0-9]*$/;
var digitos = /^\d+$/;
var textoGeneral = /^[a-zñA-ZÑÁ-Úá-úÜü _0-9\-\(\)\.\,\;\:\"\t\r\n\/%?¿]+$/;
var nombrePersona = /^[a-zA-ZÑÁ-Úá-úÜü_ ]+$/;
var usuario = /^[a-zñA-ZÑÁ-Úá-úÜü_0-9\-]+$/;
var telefono = /^[0-9 ()\-]+$/;
var telefonoETECSA = /^[0-9*#]+$/;
var siglas = /^[A-ZÑÁ-ÚÜ ]+$/;
var ci = /^((V|G|E|v|g|e)[\d]{5,10}$)|^\d{11}$/;
var pasaporte = /^([B|C|D|O][\d]{5,7}$)/;
var hora = /^([1-9]|1[0-9]):([0-5][0-9])(\s[a|p]m)$/i;
var IPAddress = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
var nombreGral = /^[a-zñA-ZÑÁ-Úá-úÜü_ ]+$/;
Ext.apply(Ext.form.VTypes, {
	//  vtype validation function
	decimal: function(val, field)
	{
		return decimal.test(val);
	}
	,
	decimalText: 'Solo n&uacute;meros..!!!',
	flotanteText: 'Solo se permiten n&uacute;meros enteros o con comas ',
	flotante: function(val, field)
	{
		return flotante.test(val);
	}
	,
	digitos: function(val, field)
	{
		return digitos.test(val);
	}
	,
	usuarioText: function(val, field)
	{
		return usuario.test(val);
	}
	,
	digitosText: 'Solo números..!!!',
	hora: function(val, field)
	{
		return hora.test(val);
	}
	,
	// vtype Text property: The error text to display when the validation function returns false
	horaText: 'No es una hora válida.  Debe tener el formato "12:34 PM".',
	// vtype Mask property: The keystroke filter mask	
	/*nombrePersona*/
	nombrePersona: function(val, field)
	{
		return nombrePersona.test(val);
	}
	,
	nombrePersonaText: 'No es un nombre v&aacute;lido.  Debe tener el formato "Nombre Apellidos".',
	/*usuario*/
	usuario: function(val, field)
	{
		return usuario.test(val);
	}
	,
	usuarioText: 'Este campo solo puede contener letras, n&uacute;meros, "-" y "_".',
	/*telefono*/
	telefono: function(val, field)
	{
		return telefono.test(val);
	}
	,
	telefonoText: 'No es un teléfono válido.  Este campo solo puede contener números, espacios, "(", ")" y "-".',
	/*textoGeneral*/
	textoGeneral: function(val, field)
	{
		return textoGeneral.test(val);
	}
	,
	textoGeneralText: 'Este campo tiene caracteres no válidos.',
	/*IP*/
	IPAddress: function(v)
	{
		return IPAddress.test(v);
	}
	,
	IPAddressText: 'Debe ser una dirección IP válida.',
	IPAddressMask: /[\d\.]/i,
	/*nombreGral*/
	nombreGral: function(val, field)
	{
		return nombreGral.test(val);
	}
	,
	nombreGralText: 'Este campo solo admite letras.',
	/*telefono*/
	telefonoETECSA: function(val, field)
	{
		return telefonoETECSA.test(val);
	}
	,
	telefonoETECSAText: 'No es un teléfono válido.  Este campo solo puede contener números, "*" y "#".'
	});

function DiferenciaFechas(fecha)
{
	//Obtiene los datos del formulario  
	CadenaFecha1 = formulario.fecha1.value
	CadenaFecha2 = FechaSistema();
	//Obtiene dia, mes y a�o  
	var fecha1 = new fecha(CadenaFecha1)
		var fecha2 = new fecha(CadenaFecha2)
		//Obtiene objetos Date  
	var miFecha1 = new Date(fecha1.anio, fecha1.mes, fecha1.dia)
		var miFecha2 = new Date(fecha2.anio, fecha2.mes, fecha2.dia)
		//Resta fechas y redondea  
	var diferencia = miFecha1.getTime() - miFecha2.getTime()
		var dias = Math.floor(diferencia / (1000 * 60 * 60 * 24))
		var segundos = Math.floor(diferencia / 1000)
		return dias;
}

function FechaSistema()
{
	var mifecha = new Date(); // Creamos el objeto para obtener la fecha y la hora
	diaMes = mifecha.getDate(); // Obtenemos el día del mes
	mes = mifecha.getMonth() + 1; // Incluimos el “+1″ ya que getMonth es una matriz que va del 0 al 11
	anyo = mifecha.getFullYear(); // Retorna la fecha en cuatro dígitos
	return fecha_sistema = diaMes + "/" + mes + "/" + anyo;
}

function fecha(cadena)
{
	//Separador para la introduccion de las fechas  
	var separador = "/"
	//Separa por dia, mes y a�o  
	if (cadena.indexOf(separador) != -1)
	{
		var posi1 = 0
		var posi2 = cadena.indexOf(separador, posi1 + 1)
			var posi3 = cadena.indexOf(separador, posi2 + 1)
			this.dia = cadena.substring(posi1, posi2)
			this.mes = cadena.substring(posi2 + 1, posi3)
			this.anio = cadena.substring(posi3 + 1, cadena.length)
		}
	else {
		this.dia = 0
		this.mes = 0
		this.anio = 0
	}
}
/*function cambiar_valor_combo(combo,valor)
{
	combo.getStore().load();
	combo.getStore().on('load',function()
	{
		combo.setValue(valor);
	}
	,combo);
}
*/

function cambiar_valor_combo_entidad (combo,valor,valor_cadena)
{
	combo.getStore().load({
		params: {
			'id_cadena': valor_cadena
		}
		});
	combo.getStore().on('load',function()
	{
		combo.setValue(valor);
	}
	,combo);
}

function cambiar_valor_combo_profesor(combo,valor,valor_departamento)
{
	combo.getStore().load({
		params: {
			'id_departamento': valor_departamento
		}
		});
	combo.getStore().on('load',function()
	{
		combo.setValue(valor);
	}
	,combo);
}

function existe(array,valor)
{
	var f = false;
	for($i = 0; $i < array.length; $i++)
	{
		if(array[$i].id_elemento == valor)
		{
			f = true;
			break;
		}
	}
	return f;
}

function ejecutar_permisos()
{
	for($i = 0; $i < permisos_global.length; $i++)
	{
		if(Ext.getCmp(permisos_global[$i].id_elemento))
		{
			Ext.getCmp(permisos_global[$i].id_elemento).enable();
		}
	}
}

function convertir_fecha(fecha)
{
	if(fecha != "")
	{
		var arrFecha = fecha.split('/');
		return arrFecha[2] + arrFecha[1] + arrFecha[0];
	}
	else
	return;
}
function validateFileExtension(fileName) {

    var exp = /^.*\.(jpg|JPG|png|PNG)$/;
    return exp.test(fileName);
    
}