function Mostrar_Grid_Buscar_Doctores() {
    if (!Ext.getCmp('grid_buscar_doctores')) {
        var grid_buscar_doctores = new Ext.grid.GridPanel({
            id: 'grid_buscar_doctores',
            layout: 'fit',
            //bodyStyle: 'padding:5px',
            height: '450',
            title: 'Listado de Doctores',
            ds: store_doctores,
            autoScroll: true,
            clicksToEdit: '1',
            columns: [
                {
                    header: "ID",
                    width: 35,
                    dataIndex: 'id',
                    sortable: true
                },
                {
                    header: "Nombres",
                    width: 120,
                    dataIndex: 'nombres',
                    sortable: true
                },
                {
                    header: "Apellidos",
                    width: 120,
                    dataIndex: 'apellidos',
                    sortable: true
                },
                {
                    header: "Ruc",
                    width: 100,
                    dataIndex: 'ruc',
                    sortable: true
                },
                {
                    header: "Celular",
                    width: 80,
                    dataIndex: 'celular',
                    sortable: true
                },
                {
                    header: "Tel&eacute;fono",
                    width: 80,
                    dataIndex: 'telefono',
                    sortable: true
                },
                {
                    header: "Correo",
                    width: 100,
                    dataIndex: 'correo',
                    sortable: true
                },
                {
                    header: "Direcci&oacute;n",
                    width: 200,
                    dataIndex: 'direccion',
                    sortable: true
                }
            ],
            selModel: new Ext.grid.RowSelectionModel({
                singleSelect: true
            })/*,
             bbar: new Ext.PagingToolbar({
             pageSize: 15,
             store: store_acciones,
             displayInfo: true,
             afterPageText: 'de {0}',
             beforePageText: 'P&aacute;gina',
             displayMsg: 'Acciones {0} - {1} de {2}',
             emptyMsg: 'No hay datos para mostrar',
             firstText: 'Primera p&aacute;gina',
             lastText: '&Uacute;ltima p&aacute;gina',
             nextText: 'P&aacute;gina siguente',
             prevText: 'P&aacute;gina anterior',
             refreshText: 'Actualizar'
             })*/,
            loadMask: true,
            tbar: [
                {
                    text: 'Modificar Doctor',
                    icon: window.App.base_url+'web/imagenes/star.png',
                    id: 'boton_modificar_doctor',
                    disabled: false,
                    handler: function (){
                        var sm = grid_buscar_doctores.getSelectionModel();
                        if (sm.getSelected()) {
                            id_doctor = sm.getSelected().get('id');
                            Mostrar_Modificar_Doctor();
                            Ext.getCmp('modificar_doctor_form').getForm().load({
                                url: 'index.php/doctor/get_doctor_por_id',
                                params: {
                                    id_doctor: id_doctor
                                },
                                waitMsg: 'Cargando..!!',
                                success: function (form, action) {
                                    data = action.result.data;
                                }
                            });
                        }
                        else {
                            Ext.Msg.alert('Alerta', 'Seleccione el Doctor a modificar');
                        }
                    }
                }
            ]
        })
        //Ext.getCmp('contenedor_buscar_acciones').add(grid_buscar_accion);
        //Ext.getCmp('contenedor_buscar_acciones').doLayout();
        var w_mostrar_buscar_doctores = new Ext.Window({
            title: 'Buscar Doctores',
            id: 'w_mostrar_buscar_doctores',
            bodyStyle: 'padding:15px;background:transparent',
            border: true,
            layout: 'form',
            modal: true,
            autoHeight: true,
            width: 1000,
            closeAction: 'close',
            resizable: true,
            items: [grid_buscar_doctores]
        });
        w_mostrar_buscar_doctores.show();
    }
}

var buscar_doctores_panel = new Ext.FormPanel({
    id: 'buscar_doctores_panel',
    layout: 'fit',
    labelAlign: 'top',
    frame: true,
    collapsible: true,
    title: 'Buscar Doctores',
    bodyStyle: 'padding:5px',
    autoScroll: true,
    overflow: 'auto',
    //width: 400,
    height: 'auto',
    defaults: {
        msgTarget: 'side',
        anchor: '95%'
    },
    items: [
        {
            defaults: {
                msgTarget: 'side',
                anchor: '95%'
            },
            layout: 'column',
            items: [
                {
                    defaults: {
                        msgTarget: 'side',
                        anchor: '95%'
                    },
                    columnWidth: .5,
                    layout: 'form',
                    items: [
                        {
                            xtype: 'textfield',
                            id: 'text_nombres_buscar_doctor',
                            fieldLabel: 'Nombres',
                            name: 'nombres',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Doctores_Submit();
                                        Mostrar_Grid_Buscar_Doctores();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            id: 'text_apellidos_buscar_doctor',
                            fieldLabel: 'Apellidos',
                            name: 'apellidos',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Doctores_Submit();
                                        Mostrar_Grid_Buscar_Doctores();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            id: 'text_ruc_buscar_doctor',
                            fieldLabel: 'Ruc',
                            name: 'ruc',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Doctores_Submit();
                                        Mostrar_Grid_Buscar_Doctores();
                                    }
                                }
                            }
                        }
                    ]
                },
                {
                    columnWidth: .5,
                    layout: 'form',
                    items: [
                        {
                            xtype: 'textfield',
                            id: 'text_correo_buscar_doctor',
                            fieldLabel: 'Correo',
                            name: 'correo',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Doctores_Submit();
                                        Mostrar_Grid_Buscar_Doctores();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            id: 'text_telefono_buscar_doctor',
                            fieldLabel: 'Tel&eacute;fono',
                            name: 'telefono',
                            vtype:'digitos',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Doctores_Submit;
                                        Mostrar_Grid_Buscar_Doctores();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            id: 'text_celular_buscar_doctor',
                            fieldLabel: 'Celular',
                            name: 'celular',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Doctores_Submit();
                                        Mostrar_Grid_Buscar_Doctores();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'textfield',
                            id: 'text_direccion_buscar_doctor',
                            fieldLabel: 'Direcci&oacute;n',
                            name: 'direccion',
                            anchor: '95%',
                            listeners: {
                                specialkey: function (field, e) {
                                    if (e.getKey() == Ext.EventObject.ENTER) {
                                        Buscar_Doctores_Submit();
                                        Mostrar_Grid_Buscar_Doctores();
                                    }
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ],

    buttons: [
        {
            text: 'Buscar',
            handler: function () {
                Buscar_Doctores_Submit();
                Mostrar_Grid_Buscar_Doctores();
            }
        }
    ]

});
function Buscar_Doctores_Submit() {
    store_doctores.baseParams.nombres = Ext.getCmp('text_nombres_buscar_doctor').getValue();
    store_doctores.baseParams.apellidos = Ext.getCmp('text_apellidos_buscar_doctor').getValue();
    store_doctores.baseParams.ruc = Ext.getCmp('text_ruc_buscar_doctor').getValue();
    store_doctores.baseParams.celular = Ext.getCmp('text_celular_buscar_doctor').getValue();
    store_doctores.baseParams.telefono = Ext.getCmp('text_telefono_buscar_doctor').getValue();
    store_doctores.baseParams.correo = Ext.getCmp('text_correo_buscar_doctor').getValue();
    store_doctores.baseParams.direccion = Ext.getCmp('text_direccion_buscar_doctor').getValue();


    store_doctores.load(/*{
     params: {
     start: 0,
     limit: 15
     }
     }*/);

}


var contenedor_buscar_doctores = {
    id: 'contenedor_buscar_doctores',
    //layout: 'fit',
    border: false,
    autoScroll: true,
    bodyStyle: 'padding:5px',
    items: [buscar_doctores_panel]
}

function Mostrar_Buscar_Doctores() {
    panel_buscar_doctor = Ext.getCmp('contentPanel-id');
    if (!Ext.getCmp('contenedor_buscar_doctores')) {
        panel_buscar_doctor.add(contenedor_buscar_doctores);
        Ext.getCmp('contentPanel-id').layout.setActiveItem('contenedor_buscar_doctores');
    }
    else {
        Ext.getCmp('contentPanel-id').layout.setActiveItem('contenedor_buscar_doctores');
    }
}

