function Mostrar_Modificar_Doctor() {
    var mostrar_modificar_doctor = new Ext.Window({
        title: 'Modificar Doctor',
        id: 'mostrar_modificar_doctor',
        bodyStyle: 'padding:15px;background:transparent',
        border: true,
        layout: 'form',

        modal: true,
        autoHeight: true,
        width: '450',

        closeAction: 'close',
        resizable: false,
        items: [modificar_doctor_form],
        buttons: [
            {
                text: 'Modificar',
                handler: function () {
                    Modificar_Doctor_Submit();
                }
            }
        ]
    });
    mostrar_modificar_doctor.show();
}

var modificar_doctor_form = {
    xtype: 'form',
    id: 'modificar_doctor_form',
    bodyStyle: 'padding:15px;background:transparent',
    border: false,
    defaultType: 'textfield',
    defaults: {
        msgTarget: 'side',
        labelStyle: 'color:#1f4981;font-weight:bold;font-size:11px;'
    },
    items: [
        {
            xtype: 'hidden',
            id: 'id_doctor',
            name: 'id'
        },
        {
            fieldLabel: 'Nombres',		//vtype: 'digitos',
            id: 'text_nombres_modificar_doctor',
            name: 'nombres',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Modificar_Doctor_Submit();
                    }
                }
            }
        },
        {
            fieldLabel: 'Apellidos',		//vtype: 'digitos',
            id: 'text_apellidos_modificar_doctor',
            name: 'apellidos',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Modificar_Doctor_Submit();
                    }
                }
            }
        },
        {
            fieldLabel: 'Ruc',
            vtype: 'digitos',
            id: 'text_ruc_modificar_doctor',
            name: 'ruc',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Modificar_Doctor_Submit();
                    }
                }
            }
        },
        {
            fieldLabel: 'Celular',
            vtype: 'digitos',
            id: 'text_celular_modificar_doctor',
            name: 'celular',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Modificar_Doctor_Submit();
                    }
                }
            }
        },
        {
            fieldLabel: 'Tel&eacute;fono',
            vtype: 'digitos',
            id: 'text_telefono_modificar_doctor',
            name: 'telefono',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Modificar_Doctor_Submit();
                    }
                }
            }
        },
        {
            fieldLabel: 'Email',		//vtype: 'digitos',
            vtype: 'email',
            name: 'correo',
            id: 'text_email_modificar_usuario',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Modificar_Doctor_Submit();
                    }
                }
            }
        },
        {
            fieldLabel: 'Direcci&oacute;n',		//vtype: 'digitos',
            id: 'text_direccion_modificar_doctor',
            name: 'direccion',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Modificar_Doctor_Submit();
                    }
                }
            }
        }
    ]
};

function Modificar_Doctor_Submit() {
    Ext.getCmp('modificar_doctor_form').getForm().submit({
        url: 'index.php/doctor/modificar_doctor',
        waitMsg: 'Modificando Doctor ...',
        success: function (fp, o) {
                Ext.Msg.show({
                    title: 'Informaci&oacute;n..!!',
                    msg: 'Se ha modificado el Doctor Correctamente',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            Ext.getCmp('mostrar_modificar_doctor').close();
            store_doctores.reload();
        }
    });
}

