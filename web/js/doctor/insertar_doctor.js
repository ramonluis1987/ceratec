function Mostrar_Insertar_Doctor() {
    var mostrar_insertar_doctor = new Ext.Window({
        title: 'Insertar Doctor',
        id: 'mostrar_insertar_doctor',
        bodyStyle: 'padding:15px;background:transparent',
        border: true,
        layout: 'form',

        modal: true,
        autoHeight: true,
        width: '450',

        closeAction: 'close',
        resizable: false,
        items: [insertar_doctor_form],
        buttons: [
            {
                text: 'Insertar',
                handler: function () {
                    Insertar_Doctor_Submit();
                }
            }
        ]
    });
    mostrar_insertar_doctor.show();
}

var insertar_doctor_form = {
    xtype: 'form',
    id: 'insertar_doctor_form',
    bodyStyle: 'padding:15px;background:transparent',
    border: false,
    defaultType: 'textfield',
    defaults: {
        msgTarget: 'side',
        labelStyle: 'color:#1f4981;font-weight:bold;font-size:11px;'
    },
    items: [
        {
            fieldLabel: 'Nombres',		//vtype: 'digitos',
            id: 'text_nombres_insertar_doctor',
            name:'nombres',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Doctor_Submit();
                    }
                }
            }
        },
        {
            fieldLabel: 'Apellidos',		//vtype: 'digitos',
            id: 'text_apellidos_insertar_doctor',
            name:'apellidos',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Doctor_Submit();
                    }
                }
            }
        },
        {
            fieldLabel: 'Ruc',
            vtype: 'digitos',
            id: 'text_ruc_insertar_doctor',
            name:'ruc',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Doctor_Submit();
                    }
                }
            }
        },{
            fieldLabel: 'Celular',
            vtype: 'digitos',
            id: 'text_celular_insertar_doctor',
            name:'celular',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Doctor_Submit();
                    }
                }
            }
        },{
            fieldLabel: 'Tel&eacute;fono',
            vtype: 'digitos',
            id: 'text_telefono_insertar_doctor',
            name:'telefono',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Doctor_Submit();
                    }
                }
            }
        },{
            fieldLabel: 'Email',		//vtype: 'digitos',
            vtype: 'email',
            name:'correo',
            id: 'text_email_insertar_usuario',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Doctor_Submit();
                    }
                }
            }
        },
        {
            fieldLabel: 'Direcci&oacute;n',		//vtype: 'digitos',
            id: 'text_direccion_insertar_doctor',
            name:'direccion',
            anchor: '95%',
            listeners: {
                specialkey: function (field, e) {
                    if (e.getKey() == Ext.EventObject.ENTER) {
                        Insertar_Doctor_Submit();
                    }
                }
            }
        }
    ]
};

function Insertar_Doctor_Submit(){
    Ext.getCmp('insertar_doctor_form').getForm().submit({
        url : 'index.php/doctor/insertar_doctor',
        waitMsg : 'Insertando Doctor ...',
        success : function(fp, o) {
            if (o.result.existe) {
                Ext.Msg.show({
                    title: 'Informaci&oacute;n..!!',
                    msg: 'Ya existe un doctor con el ruc insertado..!!',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO
                });
            }
            else{
                Ext.Msg.show({
                    title: 'Informaci&oacute;n..!!',
                    msg: 'Se ha insertado el Doctor Correctamente',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.INFO,
                    fn: function (btn) {
                        if ((btn == "ok")) {
                            Ext.Msg.show({
                                title: 'Acci&oacute;n?',
                                msg: 'Desea insertar otro Doctor?',
                                buttons: Ext.Msg.YESNO,
                                fn: function (btn) {
                                    if (btn == "yes") {
                                        Ext.getCmp('insertar_doctor_form').getForm().reset();
                                    }
                                    else {
                                        Ext.getCmp('mostrar_insertar_doctor').close();
                                    }
                                },
                                icon: Ext.MessageBox.QUESTION
                            });
                        }
                    }
                });
            }

        }
    });
}
