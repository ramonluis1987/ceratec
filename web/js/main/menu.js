var treeLoader = new Ext.tree.TreeLoader({
    dataUrl: App.base_url + 'web/data/tree.json'
});

var treePanel = new Ext.tree.TreePanel({
    id: 'tree',
    title: 'Menu',
    region: 'west',
    //margins: '5 0 5 5',
    //bodyStyle: 'padding:15px',
    height: 300,
    collapsible: true,
    //collapsed: true,
    width: 200,
    autoScroll: true,

    // tree-specific configs:
    rootVisible: false,
    lines: false,
    singleExpand: false,
    useArrows: true,
    loader: treeLoader,
    root: new Ext.tree.AsyncTreeNode(),
    items: [
        {
            xtype: "toolbar",
            labelStyle: 'font-weight:bold;',
            items: [
                {
                    xtype: "label",
                    style: "font-weight:bold",
                    height: 50,
                    text: 'Usuario: ' + usuarioData.usuario
                }
            ]
        }
    ],
    success: function () {
        //
    }
});

treePanel.on('click', function (n) {
    //n.disable();
    var sn = this.selModel.selNode ||
    {}; // selNode is null on initial selection
    if (n.leaf && n.id != sn.id) { // ignore clicks on folders and currently selected node
        Ext.getCmp('contentPanel-id').layout.setActiveItem(n.id + '-id');
    }
    if (n.id == "insertar_doctor_tree") {
        Mostrar_Insertar_Doctor();
    }
    if (n.id == "buscar_doctor_tree") {
        Mostrar_Buscar_Doctores();
    }
    if (n.id == "insertar_registro_trabajo_tree") {
        Mostrar_Insertar_Registro_Trabajo();
    }
    if (n.id == "buscar_registro_trabajo_tree") {
        mostrar_buscar_registro_trabajo();
    }
});
