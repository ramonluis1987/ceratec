function CargarLogin() {
    var loginForm = {
        xtype: 'form',
        id: 'login-form',
        bodyStyle: 'padding:15px;background:transparent',
        border: false,
        url: 'index.php/login/cargar_usuario',
        defaults: {
            msgTarget: 'side',
            labelStyle: 'color:#1f4981;font-weight:bold;font-size:11px;'
        },
        items: [
            {
                xtype: 'textfield',
                id: 'usuario',
                fieldLabel: 'Usuario',
                allowBlank: false,
                blankText: 'Entre su usuario..!!',
                minLength: 3,
                maxLength: 32,
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            Autenticar();
                        }
                    }
                }
            },
            {
                xtype: 'textfield',
                id: 'password',
                fieldLabel: 'Password',
                inputType: 'password',
                blankText: 'Entre su password..!!',
                allowBlank: false,
                minLength: 6,
                maxLength: 32,
                minLengthText: 'El password debe tener al menos 7 caracteres',
                listeners: {
                    specialkey: function (field, e) {
                        if (e.getKey() == Ext.EventObject.ENTER) {
                            Autenticar();
                        }
                    }
                }
            }
        ],
        buttons: [
            {
                text: 'Login',
                handler: function () {
                    Autenticar();

                }
            }
        ]
    }
    Ext.onReady(function () {
        win = new Ext.Window({
            id: 'mostrarLogin',
            title: 'Autenticaci&oacute;n',
            layout: 'form',
            width: 330,
            //modal: true,
            closable: false,
            autoHeight: true,
            resizable: false,
            items: [loginForm]
        });
        win.show();
    });
}

function Autenticar() {
    pass = calcMD5(Ext.getCmp('password').getValue());
    usuario = Ext.getCmp('usuario').getValue();
    Ext.Ajax.request({
        url: 'index.php/login/autenticar',
        waitMsg: 'Autenticando..!!',
        params: {
            password: pass,
            usuario: usuario

        },
        success: function (response, options) {
            usuarioData = Ext.util.JSON.decode(response.responseText);
            if (usuarioData.autenticado == true) {
                Ext.getCmp('mostrarLogin').destroy();
                CargarUsuario(CargarContenido);
            }
            else {
                Ext.Msg.show({
                    title: 'Error',
                    msg: 'Usuario y Password incorrectos..!!!',
                    buttons: Ext.Msg.OK,
                    icon: Ext.MessageBox.WARNING
                })
            }

        }
    })
}

function CargarUsuario() {
    Ext.Ajax.request({
        method: 'POST',
        url: 'index.php/login/cargar_usuario',
        success: function (response, options) {
            usuarioData = Ext.util.JSON.decode(response.responseText);
            if (usuarioData.autenticado == true) {
                permisos_global = Ext.util.JSON.decode(usuarioData.permisos);
                CargarContenido(permisos_global);
            }
            else {
                CargarLogin();
            }
            if (typeof func == "function") {
                func();
            }
        }
    });
}


