//----------------------------Menu-------------------------------
Ext.QuickTips.init();

//---------------------- variables globales-------------------------
var usuarioData = 'hola';
var permisos_global = '';

init();
//------------------------------------------------------------------

function init(func) {
    CargarUsuario();
}

function CargarContenido(p) {
    var toolbar = new Ext.Toolbar({
        id: 'idtoolbar',
        region: 'north',
        //layout:'fit',
        height: 25,
        items: [
            {
                xtype: 'button',
                icon: window.App.base_url+'web/imagenes/user.gif',
                text: 'Gestionar Usuarios',
                menu: [
                    {
                        text: 'Insertar Usuario',
                        icon: window.App.base_url+'web/imagenes/user_add.gif',
                        id: 'insertar_usuario',
                        //disabled:true,
                        handler: function () {
                            Mostrar_Usuario();
                        }
                    },
                    {
                        text: 'Buscar usuario',
                        id: 'buscar_usuario_mega',
                        //disabled:true,
                        icon: window.App.base_url+'web/imagenes/user_suit.png',
                        handler: function () {

                            Mostrar_Buscar_Usuarios();
                        }},
                    {
                        text: 'Cambiar Contrase&ntilde;a',
                        id: 'cambiar_pass',
                        icon: window.App.base_url+'web/imagenes/user_suit.png',
                        handler: function () {
                            var cambiar_pass_form = {
                                xtype: 'form',
                                id: 'cambiar_pass_form',
                                bodyStyle: 'padding:15px;background:transparent',
                                border: false,
                                defaultType: 'textfield',
                                defaults: {
                                    msgTarget: 'side',
                                    labelStyle: 'color:#1f4981;font-weight:bold;font-size:11px;'
                                },
                                items: [
                                    {
                                        allowBlank: false,
                                        fieldLabel: 'Usuario',		//vtype: 'digitos',
                                        id: 'text_usuario_cambiar_pass',
                                        anchor: '95%',
                                        disabled: true,
                                        listeners: {
                                            specialkey: function (field, e) {
                                                if (e.getKey() == Ext.EventObject.ENTER) {
                                                }
                                            }
                                        }
                                    },
                                    {
                                        allowBlank: false,
                                        fieldLabel: 'Password Actual',
                                        //vtype: 'digitos',
                                        inputType: 'password',
                                        id: 'text_old_password_cambiar_pass',
                                        blankText: 'Entre password actual..!!',
                                        minLength: 8,
                                        maxLength: 32,
                                        minLengthText: 'El password debe tener al menos 8 caracteres',
                                        anchor: '95%',
                                        listeners: {
                                            specialkey: function (field, e) {
                                                if (e.getKey() == Ext.EventObject.ENTER) {
                                                }
                                            }
                                        }
                                    },
                                    {
                                        allowBlank: false,
                                        fieldLabel: 'Password Nuevo',
                                        //vtype: 'digitos',
                                        inputType: 'password',
                                        id: 'text_new_password_cambiar_pass',
                                        blankText: 'Entre password nuevo..!!',
                                        minLength: 8,
                                        maxLength: 32,
                                        minLengthText: 'El password debe tener al menos 8 caracteres',
                                        anchor: '95%',
                                        listeners: {
                                            specialkey: function (field, e) {
                                                if (e.getKey() == Ext.EventObject.ENTER) {
                                                }
                                            }
                                        }
                                    },
                                    {
                                        allowBlank: false,
                                        fieldLabel: 'Repetir Password Nuevo',
                                        //vtype: 'digitos',
                                        inputType: 'password',
                                        id: 'text_new2_password_cambiar_pass',
                                        blankText: 'Repita el password nuevo..!!',
                                        minLength: 8,
                                        maxLength: 32,
                                        minLengthText: 'El password debe tener al menos 8 caracteres',
                                        anchor: '95%',
                                        listeners: {
                                            specialkey: function (field, e) {
                                                if (e.getKey() == Ext.EventObject.ENTER) {
                                                }
                                            }
                                        }
                                    }
                                ]

                            };

                            var mostrar_cambiar_pass = new Ext.Window({
                                title: 'Cambiar Contrase&ntilde;a',
                                id: 'mostrar_cambiar_pass',
                                bodyStyle: 'padding:15px;background:transparent',
                                border: true,
                                layout: 'form',
                                modal: true,
                                autoHeight: true,
                                width: '450',

                                closeAction: 'close',
                                resizable: false,
                                items: [cambiar_pass_form],

                                buttons: [
                                    {
                                        text: 'Cambiar',
                                        handler: function () {

                                            var usuario = Ext.getCmp('text_usuario_cambiar_pass').getValue();
                                            var password_old = calcMD5(Ext.getCmp('text_old_password_cambiar_pass').getValue());
                                            var password_new = calcMD5(Ext.getCmp('text_new_password_cambiar_pass').getValue());
                                            var password_new2 = calcMD5(Ext.getCmp('text_new2_password_cambiar_pass').getValue());
                                            if (password_new == password_new2) {
                                                Ext.Ajax.request({
                                                    url: 'index.php/accion/cambiar_pass',
                                                    waitMsg: 'Cambiando Password ...',
                                                    method: 'POST',
                                                    params: {
                                                        usuario: usuario,
                                                        password_old: password_old,
                                                        password_new: password_new
                                                    },
                                                    success: function (fp, o) {
                                                        data = Ext.util.JSON.decode(fp.responseText);
                                                        if (data.existe) {
                                                            Ext.Msg.show({
                                                                title: 'Informaci&oacute;n..!!',
                                                                msg: 'Se ha cambiado el Password Correctamente',
                                                                buttons: Ext.Msg.OK,
                                                                icon: Ext.MessageBox.INFO
                                                            });
                                                            Ext.getCmp('mostrar_cambiar_pass').close();
                                                        }
                                                        else {
                                                            Ext.Msg.show({
                                                                title: 'Informaci&oacute;n..!!',
                                                                msg: 'Usuario y Password Incorrectos..!!!',
                                                                buttons: Ext.Msg.OK,
                                                                icon: Ext.MessageBox.ERROR
                                                            });
                                                        }

                                                    }
                                                });
                                            }
                                            else {
                                                Ext.Msg.show({
                                                    title: 'Informaci&oacute;n..!!',
                                                    msg: 'Los Password nuevos deben coincidir...!!!',
                                                    buttons: Ext.Msg.OK,
                                                    icon: Ext.MessageBox.INFO
                                                });
                                            }

                                        }
                                    }
                                ]
                            });
                            Ext.getCmp('text_usuario_cambiar_pass').setValue(usuarioData.usuario);
                            mostrar_cambiar_pass.show();
                        }}
                ]
            },
            {
                xtype: 'tbfill'
            },
            {
                xtype: 'button',
                text: 'Cerrar Sesion',
                icon: window.App.base_url+'web/imagenes/user_error.gif',
                handler: function () {
                    Ext.Msg.show({
                        title: 'Confirmaci&oacute;n?',
                        msg: 'Cerrar Session..?',
                        buttons: Ext.Msg.YESNO,
                        fn: function (btn) {
                            if (btn == "yes") {
                                Ext.Ajax.request({
                                    method: 'POST',
                                    url: 'index.php/login/cerrar_session',
                                    success: function (form, action) {
                                        location.reload(true);
                                    }
                                });
                            }
                        },
                        icon: Ext.MessageBox.QUESTION
                    });
                }
            }
        ]
    });
    var treeLoader = new Ext.tree.TreeLoader({
        dataUrl: App.base_url + 'web/data/tree.json'
    });
    var treePanel = new Ext.tree.TreePanel({
        id: 'tree',
        title: 'Menu',
        region: 'west',
        //margins: '5 0 5 5',
        //bodyStyle: 'padding:15px',
        height: 300,
        collapsible: true,
        //collapsed: true,
        width: 200,
        autoScroll: true,

        // tree-specific configs:
        rootVisible: false,
        lines: false,
        singleExpand: false,
        useArrows: true,
        loader: treeLoader,
        root: new Ext.tree.AsyncTreeNode(),
        items: [
            {
                xtype: "toolbar",
                labelStyle: 'font-weight:bold;',
                items: [
                    {
                        xtype: "label",
                        style: "font-weight:bold",
                        height: 50,
                        text: 'Usuario: ' + usuarioData.usuario
                    }
                ]
            }
        ],
        success: function () {
            //
        }
    });

    treePanel.on('click', function (n) {
        //n.disable();
        var sn = this.selModel.selNode ||
        {}; // selNode is null on initial selection
        if (n.leaf && n.id != sn.id) { // ignore clicks on folders and currently selected node
            Ext.getCmp('contentPanel-id').layout.setActiveItem(n.id + '-id');
        }
        if (n.id == "insertar_doctor_tree") {
            Mostrar_Insertar_Doctor();
        }
        if (n.id == "buscar_doctor_tree") {
            Mostrar_Buscar_Doctores();
        }
        if (n.id == "insertar_registro_trabajo_tree") {
            Mostrar_Insertar_Registro_Trabajo();
        }
        if (n.id == "buscar_registro_trabajo_tree") {
            mostrar_buscar_registro_trabajo();
        }
        if (n.id == "insertar_subcategoria_tree") {
            Mostrar_Subcategoria();
        }
        if (n.id == "buscar_pedidos_tree") {
            Mostrar_Buscar_Pedidos();
        }
        if (n.id == "buscar_pedidos_cliente_tree") {
            Mostrar_Buscar_Pedidos_Cliente();
        }

    });


    function Mostrar_Fondo_Empresa() {
        panel_buscar_cuenta = Ext.getCmp('contentPanel-id');
        if (!Ext.getCmp('contenedor_fondo_empresa')) {
            panel_buscar_cuenta.add(contenedor_fondo_empresa);
            Ext.getCmp('contentPanel-id').layout.setActiveItem('contenedor_fondo_empresa');
        }
        else {
            Ext.getCmp('contentPanel-id').layout.setActiveItem('contenedor_fondo_empresa');
        }
    }
    function Mostrar_Buscar_Usuarios() {
        panel_buscar_cuenta = Ext.getCmp('contentPanel-id');
        if (!Ext.getCmp('contenedor_buscar_usuarios')) {
            panel_buscar_cuenta.add(contenedor_buscar_usuarios);
            Ext.getCmp('contentPanel-id').layout.setActiveItem('contenedor_buscar_usuarios');
        }
        else {
            Ext.getCmp('contentPanel-id').layout.setActiveItem('contenedor_buscar_usuarios');
        }
    }

    //------------------------ Contenedores generales-----------------------------

    var form_fondo_empresa = new Ext.Panel({
        bodyBorder: false,
        bodyStyle: 'padding:5px',
        html: '<div class="logo" ><img class="logo-img" src= "web/img/logo.png"/></div>'
    });

    var contenedor_buscar_usuarios = {
        id: 'contenedor_buscar_usuarios',
        //layout: 'fit',
        border: false,
        autoScroll: true,
        bodyStyle: 'padding:5px',
        items: [buscar_usuarios_panel]
    }
    var contenedor_fondo_empresa = {
     id: 'contenedor_fondo_empresa',
     //layout: 'fit',
     border: false,
     autoScroll: true,
     bodyStyle: 'padding:5px',
     items: [form_fondo_empresa]
     }
    //---------------------- Contenedor General-------------------------------------
    var contentPanel = new Ext.Panel({
        id: 'contentPanel-id',
        layout: 'card',
        region: 'center',
        activeItem: 0,
        margins: '5 5 5 0',
        items: [contenedor_fondo_empresa]
    });

    //------------------------------- ViewPort--------------------------------------
    Ext.onReady(function () {
        var container = new Ext.Viewport({
            layout: 'border',
            bodyStyle: 'padding:15px',
            autoScroll: true,
            renderTo: Ext.getBody(),
            defaults: {
                collapsible: false,
                split: true,
                margins: '5 0 5 5'
            },
            items: [contentPanel, treePanel, toolbar]
        });
    });
}
