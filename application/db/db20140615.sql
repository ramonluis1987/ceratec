# SQL Manager 2007 for MySQL 4.1.2.1
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : registro_trabajo


SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE `registro_trabajo`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `registro_trabajo`;

#
# Structure for the `ci_sessions` table : 
#

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

#
# Structure for the `n_estados` table : 
#

CREATE TABLE `n_estados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

#
# Structure for the `t_doctores` table : 
#

CREATE TABLE `t_doctores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `ruc` int(11) DEFAULT NULL,
  `celular` int(11) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `ruc` (`ruc`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Structure for the `t_estado_registro_trabajo` table : 
#

CREATE TABLE `t_estado_registro_trabajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_registro_trabajo` int(11) DEFAULT NULL,
  `observaciones` varchar(350) DEFAULT NULL,
  `id_estado` int(11) DEFAULT NULL,
  `factura` varchar(100) DEFAULT NULL,
  `fecha_entrada` varchar(20) DEFAULT NULL,
  `fecha_entrega` varchar(20) DEFAULT NULL,
  `hora_entrega` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

#
# Structure for the `t_registro_trabajo` table : 
#

CREATE TABLE `t_registro_trabajo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_entrada` varchar(20) DEFAULT NULL,
  `paciente` varchar(200) DEFAULT NULL,
  `id_doctor` int(11) DEFAULT NULL,
  `clinica` varchar(200) DEFAULT NULL,
  `fecha_entrega` varchar(20) DEFAULT NULL,
  `hora_entrega` varchar(20) DEFAULT NULL,
  `orden` varchar(200) DEFAULT NULL,
  `trabajador` varchar(200) DEFAULT NULL,
  `id_estado_actual` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `orden` (`orden`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

#
# Structure for the `t_usuario` table : 
#

CREATE TABLE `t_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidos` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `activo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

#
# Data for the `ci_sessions` table  (LIMIT 0,500)
#

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES 
  ('c140b916c1dbe45102095b7391419bb0','::1','Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36',1402893451,'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"usuario\";s:5:\"ramon\";s:11:\"autenticado\";b:1;s:6:\"nombre\";s:26:\"Ramón Luis Morales Vega\";s:10:\"id_usuario\";s:1:\"1\";}');

COMMIT;

#
# Data for the `n_estados` table  (LIMIT 0,500)
#

INSERT INTO `n_estados` (`id`, `nombre`) VALUES 
  (1,'Entrada Inicial'),
  (2,'Salida a Prueba'),
  (3,'Entrada de Prueba'),
  (4,'Terminado'),
  (5,'Pagado');

COMMIT;

#
# Data for the `t_doctores` table  (LIMIT 0,500)
#

INSERT INTO `t_doctores` (`id`, `nombres`, `apellidos`, `ruc`, `celular`, `telefono`, `correo`, `direccion`) VALUES 
  (1,'sdfsd','rer',34534,34534,3453,'sf@asfdsa.fgjf','dgdfgdf'),
  (2,'dfgdfg','dfg',234324,23434,45345,'sdfsd@efse.com','sdfsdf'),
  (3,'Ramon','Morales',324234,34534,3453,'sf@asfdsa.fgjf','zcxvxc'),
  (4,'Jose','Perez',2147483647,995767951,6019919,'ramonluis1987@outlook.com','Ave Prensa');

COMMIT;

#
# Data for the `t_estado_registro_trabajo` table  (LIMIT 0,500)
#

INSERT INTO `t_estado_registro_trabajo` (`id`, `id_registro_trabajo`, `observaciones`, `id_estado`, `factura`, `fecha_entrada`, `fecha_entrega`, `hora_entrega`) VALUES 
  (1,12,'sfsfs',1,'234324','2014-06-15',NULL,NULL),
  (2,13,'dgdfg',1,'2343243','2014-06-16',NULL,NULL),
  (3,14,'para los dientes',1,NULL,NULL,NULL,NULL),
  (4,15,'fdsdfsdfsf',1,NULL,'2014-06-16 00:55:44','2014-06-15','1:00 AM'),
  (7,15,'zxczxczxczxc',2,NULL,NULL,'2014-06-16 06:37:45',NULL),
  (8,14,'zczxcz',2,NULL,NULL,'2014-06-16 06:40:23',NULL);

COMMIT;

#
# Data for the `t_registro_trabajo` table  (LIMIT 0,500)
#

INSERT INTO `t_registro_trabajo` (`id`, `fecha_entrada`, `paciente`, `id_doctor`, `clinica`, `fecha_entrega`, `hora_entrega`, `orden`, `trabajador`, `id_estado_actual`) VALUES 
  (1,'2014-06-19',NULL,4,NULL,NULL,NULL,NULL,NULL,1),
  (2,'2014-06-13','dsfsdf',3,'dfsdf','2014-06-19 00:00:00','1:45 AM','dsfdsf','dfsf',1),
  (3,'2014-06-13','dsfsdf',2,'xzczxc','2014-06-18 00:00:00','1:45 AM','zxczxzx','czx',1),
  (4,'2014-06-13 07:02:44','zcczxc',1,'zxczxc','2014-06-25 00:00:00','2:00 AM','zczczxc','zxczxczxc',1),
  (5,'2014-06-13 07:04:03','zxczxc',3,'zxczxc','2014-06-18','2:00 AM','asdasd','asdaasd',1),
  (6,'2014-06-14 20:23:09','fsdfsdf',2,'sdfsdf','2014-06-10','12:45 AM','sdfsdf','fsdfsd',1),
  (7,'2014-06-14 20:36:16','sdfsdfsd',2,'sdfsd','2014-06-10','1:00 AM','sfsdf','sdfsdf',5),
  (8,'2014-06-14 20:38:13','zsczdfsd',2,'sdfsdf','2014-06-12','12:30 AM','sdfsd','fsdfsd',4),
  (9,'2014-06-14 20:42:36','sdfsdfsdf',3,'sdfsdfsdf','2014-06-10','1:00 AM','sdfsdfsd','fsdf',3),
  (10,'2014-06-15 21:35:42','sdfsf',2,'sdfsdf','2014-06-19','12:45 AM','dsfsdf','sfsdf',2),
  (11,'2014-06-15 21:39:15','sdfsf',2,'sdfsdf','2014-06-19','12:45 AM','sdsdsfsdfsdfsdg','sfsdf',1),
  (12,'2014-06-15 21:45:29','sdfsf',2,'sdfsd','2014-06-19','12:45 AM','poiuytree','sfsdf',2),
  (13,'2014-06-15 22:06:07','fdgdfgdfg',3,'dfgdfg','2014-06-15','1:00 AM','sdfgdg','dfgdfgdf',1),
  (14,'2014-06-16 00:33:10','Ramon Morales',3,'Mi Casa','2014-06-30','12:00 AM','111','Pepito',2),
  (15,'2014-06-16 00:55:44','sdfdsfsdf',2,'sdfdsf','2014-06-15','1:00 AM','sdf','sdfss',2);

COMMIT;

#
# Data for the `t_usuario` table  (LIMIT 0,500)
#

INSERT INTO `t_usuario` (`id_usuario`, `usuario`, `password`, `nombre`, `apellidos`, `email`, `telefono`, `activo`) VALUES 
  (1,'ramon','5b766f8afb4c7cecc215daf5a5bc511c','Ramón Luis','Morales Vega','ramonluis1987@outlook.com',NULL,1),
  (2,'pepe','5b766f8afb4c7cecc215daf5a5bc511c','Pepe','Morales','ramonluis1987@outlook.com','026019919',1),
  (3,'dianelis','94c02fa77f0aa8415f94819991f30dc2','Dianelis','Delgado Corrales','dianeliscorrales@yahoo.es','026019919',1),
  (4,'andres','1e54e1911700e4f5496d2b739b98eda7','Andres','Espinel','ceratec2012@hotmail.com','2244175',1);

COMMIT;
