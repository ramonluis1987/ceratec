<?php

class T_registro_trabajo_model extends CI_Model
{
    var $fecha_entrada = '';
    var $paciente = '';
    var $id_doctor = '';
    var $clinica = '';
    var $fecha_entrega = '';
    var $hora_entrega = '';
    var $orden = '';
    var $trabajador = '';
    var $id_estado_actual = 1;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function existe_orden()
    {
        $orden = $_POST ['orden'];
        $this->db->select('id');
        $this->db->from('t_registro_trabajo');
        $this->db->where('orden', $orden);
        $query = $this->db->get();
        $success = false;
        if ($query->num_rows() > 0) {
            $success = true;
        }
        return $success;
    }

    public function parametros()
    {
        date_default_timezone_set('America/Guayaquil');
        $this->fecha_entrada = date("Y-m-d H:i:s");
        if (!empty($_POST['paciente'])) {
            $this->paciente = $_POST['paciente'];
        }
        if (!empty($_POST['id_doctor'])) {
            $this->id_doctor = $_POST['id_doctor'];
        }
        if (!empty($_POST['clinica'])) {
            $this->clinica = $_POST['clinica'];
        }
        if (!empty($_POST['fecha_entrega'])) {
            $this->fecha_entrega = $_POST['fecha_entrega'];
        }
        if (!empty($_POST['hora_entrega'])) {
            $this->hora_entrega = $_POST['hora_entrega'];
        }
        if (!empty($_POST['orden'])) {
            $this->orden = $_POST['orden'];
        }
        if (!empty($_POST['trabajador'])) {
            $this->trabajador = $_POST['trabajador'];
        }
    }

    public function insertar_registro_trabajo()
    {
        $this->parametros();
        $success = $this->db->insert('t_registro_trabajo', $this);
        $data = array('id_registro_trabajo' => $this->db->insert_id(), 'observaciones' => $_POST['observaciones'], 'id_estado' => 1,
            'fecha_entrada'=>$this->fecha_entrada,'fecha_entrega'=>$this->fecha_entrega,'hora_entrega'=>$this->hora_entrega);
        $this->insertar_estado_registro_trabajo($data);
        return $success;
    }

    function insertar_estado_registro_trabajo($data)
    {
        $this->db->insert('t_estado_registro_trabajo', $data);
    }

    /*public function modificar_registro_trabajo()
    {
        $id_registro_trabajo = $_POST['id'];
        $this->parametros();
        $this->db->where('id', $registro_trabajo);
        return $this->db->update('t_registro_trabajo', $this);
    }*/

    function get_registro_trabajos()
    {
        $this->parametros();

        //$limit = $_POST['limit'];
        //$start = $_POST['start'];
        //$this->db->start_cache();
        $this->db->select('t_registro_trabajo.*,t_doctores.nombres as nombre_doctor,n_estados.nombre as nombre_estado,
        n_estados.id as id_estado,t_estado_registro_trabajo.observaciones');
        $this->db->join('t_doctores', 't_doctores.id = t_registro_trabajo.id_doctor');
        $this->db->join('n_estados', 'n_estados.id = t_registro_trabajo.id_estado_actual');
        $this->db->join('t_estado_registro_trabajo', 't_estado_registro_trabajo.id_registro_trabajo = t_registro_trabajo.id');
        //$this->db->from('t_registro_trabajo');

        /* if ($this->fecha != '') {
             $this->db->like('fecha', $this->fecha);
         }*/
        if ($_POST['fecha_entrega_desde'] != '' && $_POST['fecha_entrega_hasta'] != '') {
            $this->db->where('fecha_entrega >=', $_POST['fecha_entrega_desde']);
            $this->db->where('fecha_entrega <=', $_POST['fecha_entrega_hasta']);
        }
        if ($this->paciente != '') {
            $this->db->like('paciente', $this->paciente);
        }
        if ($this->id_doctor != '') {
            $this->db->where('id_doctor', $this->id_doctor);
        }
        if ($this->clinica != '') {
            $this->db->like('clinica', $this->clinica);
        }
        if ($this->fecha_entrega != '') {
            $this->db->like('fecha_entrega', $this->fecha_entrega);
        }
        if ($this->hora_entrega != '') {
            $this->db->like('hora_entrega', $this->hora_entrega);
        }
        if ($this->orden != '') {
            $this->db->like('orden', $this->orden);
        }
        if ($this->trabajador != '') {
            $this->db->like('trabajador', $this->trabajador);
        }
        if ($_POST['id_estado'] != '') {
            $this->db->like('id_estado_actual', $_POST['id_estado']);
        }
        if ($_POST['observaciones'] != '') {
            $this->db->like('t_estado_registro_trabajo.observaciones', $_POST['observaciones']);
        }
        $this->db->where('t_estado_registro_trabajo.id_estado',1);
        $this->db->order_by("t_registro_trabajo.id", "asc");
        $query = $this->db->get('t_registro_trabajo');
        $arr_result = $query->result_array();
        return $arr_result;
    }

    function get_registro_trabajo_por_id()
    {
        $id_registro_trabajo = $_POST['id_registro_trabajo'];
        $this->db->select('*');
        $this->db->from('t_registro_trabajo');
        $this->db->where('id', $id_registro_trabajo);
        $query = $this->db->get();
        $obj = $query->row(1);
        return $obj;
    }

    function cargar_estados()
    {
        $this->db->select('id,nombre');
        $query = $this->db->get('n_estados');
        return $query->result_array();
    }

    function get_estados_registro_trabajo()
    {
        $this->db->select('t_estado_registro_trabajo.*,n_estados.nombre as nombre_estado');
        $this->db->from('t_estado_registro_trabajo');
        $this->db->where('id_registro_trabajo',$_POST['id_registro_trabajo']);
        $this->db->join('n_estados', 'n_estados.id = t_estado_registro_trabajo.id_estado');
        $query = $this->db->get();
        return $query->result_array();
    }

    function cambiar_estado()
    {
        date_default_timezone_set('America/Guayaquil');
        $id_estado = $_POST['id'];
        if($id_estado == 2)
        {
            $data = array('id_registro_trabajo'=>$_POST['id_registro_trabajo'],'id_estado'=>$id_estado,'observaciones'=>$_POST['observaciones']
            ,'fecha_entrega'=>date("Y-m-d H:i:s"),'fecha_entrada'=>date("Y-m-d H:i:s") );
            $success = $this->db->insert('t_estado_registro_trabajo',$data);
            $this->cambiar_estado_actual($_POST['id_registro_trabajo'],2);

            $data_fecha = array('fecha_entrega'=>date("Y-m-d H:i:s"),'fecha_entrada'=>date("Y-m-d H:i:s"),'hora_entrega'=>'');
            $this->db->where('id',$_POST['id_registro_trabajo']);
            $this->db->update('t_registro_trabajo',$data_fecha);
        }
        if($id_estado == 3)
        {
            $data = array('id_registro_trabajo'=>$_POST['id_registro_trabajo'],'id_estado'=>$id_estado,'observaciones'=>$_POST['observaciones']
            ,'fecha_entrada'=>date("Y-m-d H:i:s"),'fecha_entrega'=>$_POST['fecha_entrega'],'hora_entrega'=>$_POST['hora_entrega'] );
            $success = $this->db->insert('t_estado_registro_trabajo',$data);
            $this->cambiar_estado_actual($_POST['id_registro_trabajo'],3);
            $data_fecha = array('fecha_entrada'=>date("Y-m-d H:i:s"),'fecha_entrega'=>$_POST['fecha_entrega'],'hora_entrega'=>$_POST['hora_entrega']);
            $this->db->where('id',$_POST['id_registro_trabajo']);
            $this->db->update('t_registro_trabajo',$data_fecha);
        }
        if($id_estado == 4)
        {
            $data = array('id_registro_trabajo'=>$_POST['id_registro_trabajo'],'id_estado'=>$id_estado,'observaciones'=>$_POST['observaciones']
            ,'fecha_entrada'=>date("Y-m-d H:i:s"),'fecha_entrega'=>date("Y-m-d H:i:s"));
            $success = $this->db->insert('t_estado_registro_trabajo',$data);
            $this->cambiar_estado_actual($_POST['id_registro_trabajo'],4);
            $data_fecha = array('fecha_entrega'=>date("Y-m-d H:i:s"),'fecha_entrada'=>date("Y-m-d H:i:s"),'hora_entrega'=>'');
            $this->db->where('id',$_POST['id_registro_trabajo']);
            $this->db->update('t_registro_trabajo',$data_fecha);
        }
        if($id_estado == 5)
        {
            $data = array('id_registro_trabajo'=>$_POST['id_registro_trabajo'],'id_estado'=>$id_estado,'observaciones'=>$_POST['observaciones']
            ,'fecha_entrada'=>date("Y-m-d H:i:s"),'fecha_entrega'=>date("Y-m-d H:i:s"),'factura'=>$_POST['factura']);
            $success = $this->db->insert('t_estado_registro_trabajo',$data);
            $this->cambiar_estado_actual($_POST['id_registro_trabajo'],5);
            $data_fecha = array('fecha_entrega'=>date("Y-m-d H:i:s"),'fecha_entrada'=>date("Y-m-d H:i:s"),'hora_entrega'=>'');
            $this->db->where('id',$_POST['id_registro_trabajo']);
            $this->db->update('t_registro_trabajo',$data_fecha);
        }
        return $success;
    }

    function cambiar_estado_actual($id_registro_trabajo,$id_estado)
    {
        $this->db->where('id',$id_registro_trabajo);
        $this->db->update('t_registro_trabajo',array('id_estado_actual'=>$id_estado));
    }
}

?>