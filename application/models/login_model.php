<?php
class login_model extends CI_Model{
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	function get_autenticacion()
    {
        $usuario = $_POST['usuario'];
		$password = md5($_POST['password']);

		$this->load->database();
		$this->db->select('t_profesor.usuario,t_rol.id_rol,t_profesor.nombre_profesor,t_profesor.apellidos');
		$this->db->from('t_profesor');
		$this->db->join('t_rol','t_profesor.id_rol = t_rol.id_rol');
		$this->db->where('usuario', $usuario);
		$this->db->where('pass', $password);
        $query = $this->db->get();
        return $query->first_row();
    }
}
?>