<?php
class T_usuario_model extends CI_Model{
	var $usuario = '';
	var $password = '';
	var $nombre = '';
	var $apellidos = '';
	var $email = '';
    var $telefono = '';
	var $activo = 0;

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
        $this->load->database();
	}
	function get_autenticacion()
    {
        $usuario = $_POST['usuario'];
		$password = md5($_POST['password']);
		$this->db->select('t_usuario.id_usuario,t_usuario.usuario,t_usuario.nombre,t_usuario.apellidos');
		$this->db->from('t_usuario');
		$this->db->where('usuario', $usuario);
		$this->db->where('password', $password);
        $query = $this->db->get();
        return $query->first_row();
    }
    public function existe_usuario()
    {
        $usuario = $_POST ['usuario'];
        $this->load->database ();
        $this->db->select ( 'usuario' );
        $this->db->where ( 'usuario',$usuario);
        $query = $this->db->get ( 't_usuario' );
        $success = false;
        if ($query->num_rows () > 0) {
            $success = true;
        }
        return $success;
    }
    public function cambiar_pass()
    {
        $usuario = $_POST['usuario'];
        $password_old = md5($_POST['password_old']);
        $password_new = md5($_POST['password_new']);
        $this->db->select('t_usuario.id_usuario');
        $this->db->from('t_usuario');
        //$this->db->join('t_rol','t_profesor.id_rol = t_rol.id_rol');
        $this->db->where('usuario', $usuario);
        $this->db->where('password', $password_old);
        $query = $this->db->get();
        $success = false;
        if ($query->num_rows() > 0)
        {
            $this->db->where('usuario', $usuario);
            $success = $this->db->update('t_usuario',array('password'=>$password_new));
        }
        return $success;
    }
    public function parametros()
    {
        if(!empty($_POST['usuario']))
        {
            $this->usuario = $_POST['usuario'];
        }
        if(!empty($_POST['password']))
        {
            $this->password = md5($_POST['password']) ;
        }
        if(!empty($_POST['nombre']))
        {
            $this->nombre = $_POST['nombre'];
        }
        if(!empty($_POST['apellidos']))
        {
            $this->apellidos = $_POST['apellidos'];
        }
        if(!empty($_POST['email']))
        {
            $this->email = $_POST['email'];
        }
        if(!empty($_POST['telefono']))
        {
            $this->telefono = $_POST['telefono'];
        }
        if(!empty($_POST['activo']))
        {
            $this->activo = $_POST['activo'];
        }

    }
	public function insertar_usuario()
	{
        $this->parametros();
		return $this->db->insert('t_usuario',$this);
	}
    public function modificar_usuario()
    {
        $id_usuario = $_POST['id_usuario'];
        $this->parametros();
        $data = array('usuario'=> $this->usuario,'nombre'=>$this->nombre,'apellidos'=>$this->apellidos,'email'=>$this->email,
        'telefono'=>$this->telefono,'activo'=>$this->activo);
        $this->db->where('id_usuario',$id_usuario);
        return $this->db->update('t_usuario',$data);
    }
	function get_usuarios()
	{
		if(!empty($_POST['usuario']))
		{
			$this->usuario = $_POST['usuario'];
		}
		if(!empty($_POST['nombre']))
		{
			$this->nombre = $_POST['nombre'];
		}
		if(!empty($_POST['apellidos']))
		{
			$this->apellidos = $_POST['apellidos'];
		}
		if(!empty($_POST['email']))
		{
			$this->email = $_POST['email'];
		}
        if(!empty($_POST['telefono']))
        {
            $this->telefono = $_POST['telefono'];
        }
		if(!empty($_POST['activo']))
		{
			$this->activo = $_POST['activo'];
		}
		
		//$limit = $_POST['limit'];
		//$start = $_POST['start'];
		//$this->db->start_cache();
		$this->db->select('t_usuario.id_usuario,t_usuario.usuario,t_usuario.nombre,t_usuario.apellidos,t_usuario.email,t_usuario.telefono
		,t_usuario.activo');
		$this->db->from('t_usuario');

		if($this->usuario != '')
		{
			$this->db->like('t_usuario.usuario',$this->usuario);
		}
		if($this->nombre != '')
		{
			$this->db->like('t_usuario.nombre',$this->nombre);
		}
		if($this->apellidos != '')
		{
			$this->db->like('t_usuario.apellidos',$this->apellidos);
		}
		if($this->email != '')
		{
			$this->db->like('t_usuario.email',$this->email);
		}
        if($this->telefono != '')
        {
            $this->db->like('t_usuario.telefono',$this->telefono);
        }
		if($this->activo != 0 )
		{
			$this->db->where('t_usuario.activo',$this->activo);
		}
		$this->db->order_by("id_usuario", "asc");
		$query = $this->db->get();
		$arr_result = $query->result_array();
		return $arr_result;
	}
	function get_usuario_by_id()
	{
		$id_usuario = $_POST['id_usuario'];
		$this->db->select('t_usuario.id_usuario,t_usuario.usuario,t_usuario.nombre,t_usuario.apellidos,t_usuario.email,t_usuario.telefono
		,t_usuario.activo,');
		$this->db->from('t_usuario');
		$this->db->where('id_usuario',$id_usuario);
		$query = $this->db->get();
        $obj = $query->row(1);
        return $obj;
	}
}
?>