<?php

class T_doctor_model extends CI_Model
{
    var $nombres = '';
    var $apellidos = '';
    var $ruc = '';
    var $celular = '';
    var $telefono = '';
    var $correo = '';
    var $direccion = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function existe_ruc()
    {
        $ruc = $_POST ['ruc'];
        $success = false;
        if($ruc == '')
        {
            return false;
        }
        else{
            $this->db->select('id');
            $this->db->where('ruc', $ruc);
            $query = $this->db->get('t_doctores');
            if ($query->num_rows() > 0) {
                $success = true;
            }
        }

        return $success;
    }

    public function parametros()
    {

        if (!empty($_POST['nombres'])) {
            $this->nombres = $_POST['nombres'];
        }
        if (!empty($_POST['apellidos'])) {
            $this->apellidos = $_POST['apellidos'];
        }
        if (!empty($_POST['ruc'])) {
            $this->ruc = $_POST['ruc'];
        }
        if (!empty($_POST['celular'])) {
            $this->celular = $_POST['celular'];
        }
        if (!empty($_POST['telefono'])) {
            $this->telefono = $_POST['telefono'];
        }
        if (!empty($_POST['correo'])) {
            $this->correo = $_POST['correo'];
        }
        if (!empty($_POST['direccion'])) {
            $this->direccion = $_POST['direccion'];
        }

    }

    public function insertar_doctor()
    {
        $this->parametros();
        return $this->db->insert('t_doctores', $this);
    }

    public function modificar_doctor()
    {
        $id_doctor = $_POST['id'];
        $this->parametros();
        $this->db->where('id',$id_doctor);
        return $this->db->update('t_doctores', $this);
    }

    public function modificar_usuario()
    {
        $id_usuario = $_POST['id_usuario'];
        $this->parametros();
        $data = array('usuario' => $this->usuario, 'nombre' => $this->nombre, 'apellidos' => $this->apellidos, 'email' => $this->email,
            'telefono' => $this->telefono, 'activo' => $this->activo);
        $this->db->where('id_usuario', $id_usuario);
        return $this->db->update('t_usuario', $data);
    }

    function get_doctores()
    {
        $this->parametros();

        //$limit = $_POST['limit'];
        //$start = $_POST['start'];
        //$this->db->start_cache();
        $this->db->select('*');
        $this->db->from('t_doctores');

        if ($this->nombres != '') {
            $this->db->like('nombres', $this->nombres);
        }
        if ($this->apellidos != '') {
            $this->db->like('apellidos', $this->apellidos);
        }
        if ($this->ruc != '') {
            $this->db->like('ruc', $this->ruc);
        }
        if ($this->celular != '') {
            $this->db->like('celular', $this->celular);
        }
        if ($this->telefono != '') {
            $this->db->like('telefono', $this->telefono);
        }
        if ($this->correo != '') {
            $this->db->like('correo', $this->correo);
        }
        if ($this->direccion != '') {
            $this->db->like('direccion', $this->direccion);
        }
        $this->db->order_by("id", "asc");
        $query = $this->db->get();
        $arr_result = $query->result_array();
        return $arr_result;
    }

    function get_doctor_por_id()
    {
        $id_doctor = $_POST['id_doctor'];
        $this->db->select('*');
        $this->db->from('t_doctores');
        $this->db->where('id', $id_doctor);
        $query = $this->db->get();
        $obj = $query->row(1);
        return $obj;
    }

    function cargar_doctores()
    {
       $this->db->select('id,CONCAT(nombres, " ",apellidos) as nombres',false);
        if($_POST['query'] != '')
        {
            $this->db->like('nombres ',$_POST['query']);
        }
        $query = $this->db->get('t_doctores');
        return $query->result_array();
    }
}

?>