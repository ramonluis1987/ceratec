<?php
ini_set('display_errors', 1);
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Registro_trabajo extends CI_Controller
{
    public function index()
    {

    }

    public function insertar_registro_trabajo()
    {
        $this->load->model('T_registro_trabajo_model');
        $orden = $_POST['orden'];
        if ($this->T_registro_trabajo_model->existe_orden($orden)) {
            echo "{\"existe\":true,\"success\":true}";
        } else {
            if ($this->T_registro_trabajo_model->insertar_registro_trabajo()) {
                echo "{\"existe\":false,\"success\":true}";
            }
        }
    }

    public function get_estados_registro_trabajo()
    {
        $this->load->model('T_registro_trabajo_model');
        echo "{\"data\":" . json_encode($this->T_registro_trabajo_model->get_estados_registro_trabajo()) . "}";
    }

    public function get_registros_trabajos()
    {
        $this->load->model('T_registro_trabajo_model');
        echo "{\"data\":" . json_encode($this->T_registro_trabajo_model->get_registro_trabajos()) . "}";
    }

    public function cargar_estados()
    {
        $this->load->model('T_registro_trabajo_model');
        echo "{\"data\":" . json_encode($this->T_registro_trabajo_model->cargar_estados()) . "}";
    }

    public function cambiar_estado()
    {
        $this->load->model('T_registro_trabajo_model');
        if ($this->T_registro_trabajo_model->cambiar_estado()) {
            echo "{\"success\":true}";
        } else {

            echo "{\"success\":true}";
        }
    }
    public  function  exportar_excel()
    {
        $this->load->model('T_proforma_model');
        // echo json_encode($this->T_proforma_model->get_proforma_excel($id_pedido)['productos']);
        $this->load->library('PHPExcel');
        $this->load->library('PHPExcel/IOFactory');
        $proforma = $this->T_proforma_model->get_proforma_excel($id_pedido);
        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
            ->setDescription("description");

        // Assign cell values


        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Ing. Ramon Luis Morales Vega")
            ->setLastModifiedBy("Ramon Luis")
            ->setTitle("Registros de Traabajo")
            ->setSubject("Registros de Traabajo")
            ->setDescription("Registros de Traabajo")
            ->setKeywords("office 2007")
            ->setCategory("Registros de Traabajo");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Proforma Pedido '.$proforma['proforma']->id_pedido);
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Descuento');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Precio');

        //$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Apellidos');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:B1');
        $pos = 0;
        $count_productos = count($proforma['productos']);
        for ($i = 0; $i < $count_productos; $i++) {
            $pos = $i + 2;
            //echo 'A'.$pos;
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $pos, $proforma['productos'][$i]['marca']);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $pos, $proforma['productos'][$i]['modelo']);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $pos, $proforma['productos'][$i]['detalles']);
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $pos, $proforma['productos'][$i]['descuento_valor']);
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $pos, $proforma['productos'][$i]['precio']);
        }

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client�s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="registro de trabajo.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }
}