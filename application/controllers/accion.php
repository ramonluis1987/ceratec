<?php

if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Accion extends CI_Controller
{
    public function index()
    {
    }

    public function insertar_descuento_producto()
    {
        $this->load->model('T_proforma_model');
        $this->T_proforma_model->modificar_descuento();
    }

    public function cargar_subcategoria()
    {
        $this->load->model('N_subcategoria_model');
        echo "{\"data\":" . json_encode($this->N_subcategoria_model->get_subcategorias()) . "}";
    }

    public function cargar_categoria()
    {
        $this->load->model('N_categoria_model');
        echo "{\"data\":" . json_encode($this->N_categoria_model->get_categorias()) . "}";
    }

    public function cargar_estados()
    {
        $this->load->model('N_estado_model');
        echo "{\"data\":" . json_encode($this->N_estado_model->get_estados()) . "}";
    }

    public function cargar_tipo_usuario()
    {
        $this->load->model('N_tipo_usuario_model');
        echo "{\"data\":" . json_encode($this->N_tipo_usuario_model->get_tipo_usuarios()) . "}";
    }

    public function cargar_producto_pedido_tmp()
    {
        $this->load->model('T_pedido_producto_tmp_model');
        echo "{\"data\":" . json_encode($this->T_pedido_producto_tmp_model->get_pedido_tmp()) . "}";
    }

    public function cargar_pedidos()
    {
        $this->load->model('T_pedido_producto_tmp_model');
        echo "{\"data\":" . json_encode($this->T_pedido_producto_tmp_model->get_pedidos()) . "}";
    }

    public function cargar_pedidos_cliente()
    {
        $this->load->model('T_pedido_producto_tmp_model');
        echo "{\"data\":" . json_encode($this->T_pedido_producto_tmp_model->get_pedidos_cliente()) . "}";
    }

    public function cargar_pedidos_productos()
    {
        $this->load->model('T_pedido_producto_tmp_model');
        echo "{\"data\":" . json_encode($this->T_pedido_producto_tmp_model->get_pedidos_productos()) . "}";
    }

    public function cargar_operador()
    {
        $this->load->model('T_usuario_model');
        echo "{\"data\":" . json_encode($this->T_usuario_model->get_operadores()) . "}";
    }

    public function detalles_empleado()
    {
        $this->load->model('T_empleados_model');
        echo "{success:true,data:" . json_encode($this->T_empleados_model->get_empleado_por_id()) . "}";
    }
    public function cambiar_pass()
    {
        $this->load->model('T_usuario_model');
        if ($this->T_usuario_model->cambiar_pass()) {
            echo "{\"existe\":true,\"success\":true}";
        } else {
            echo "{\"existe\":false,\"success\":true}";;
        }
    }
    public function insertar_producto()
    {
        $this->load->model('T_producto_model');
        $codigo1 = $_POST['codigo1'];
        if ($this->T_producto_model->existe_codigo($codigo1)) {
            echo "{\"existe\":true,\"success\":true}";
        } else {
            if ($this->T_producto_model->insertar_producto()) {
                echo "{\"existe\":false,\"success\":true}";
            }
        }
    }

    public function insertar_usuario()
    {
        $this->load->model('T_usuario_model');
        if($this->T_usuario_model->existe_usuario())
        {
            echo "{\"existe\":true,\"success\":true}";
        }else  if ($this->T_usuario_model->insertar_usuario()) {
            echo "{\"existe\":false,\"success\":true}";
        }

    }
    public function modificar_usuario()
    {
        $this->load->model('T_usuario_model');
        if ($this->T_usuario_model->modificar_usuario()) {
            echo "{\"success\":true}";
        } else {
            echo "{\"success\":false}";
        }
    }

    public function insertar_categoria()
    {
        $this->load->model('N_categoria_model');
        if ($this->N_categoria_model->insertar_categoria()) {
            echo "{\"success\":true}";
        } else {
            echo "{\"success\":false}";
        }
    }

    public function insertar_pedido_producto_tmp()
    {
        $this->load->model('T_pedido_producto_tmp_model');
        if (!$this->T_pedido_producto_tmp_model->existe_producto_pedido()) {
            if ($this->T_pedido_producto_tmp_model->insertar_pedido_producto_tmp()) {
                echo "{existe:false,\"success\":true}";
            } else {
                echo "{\"success\":false}";
            }
        } else {
            echo "{existe:true,\"success\":true}";
        }
    }

    public function insertar_proforma()
    {
        $this->load->model('T_proforma_model');
        if ($this->T_proforma_model->insertar_proforma()) {
            echo "{\"success\":true}";
        } else {
            echo "{\"success\":false}";
        }
    }
    public function modificar_proforma()
    {
        $this->load->model('T_proforma_model');
        if ($this->T_proforma_model->modificar_proforma()) {
            echo "{\"success\":true}";
        } else {
            echo "{\"success\":false}";
        }
    }
    public function existe_proforma()
    {
        $this->load->model('T_proforma_model');
        $result = $this->T_proforma_model->existe_proforma();
        if ($result['existe']) {
            echo "{existe:true,\"success\":true,estado:" . $result['estado'] . ",id_proforma:" . $result['id_proforma'] . "}";
        } else {
            echo "{existe:false,\"success\":true}";
        }
    }
    public function hacer_pedido()
    {
        $this->load->model('T_pedido_producto_tmp_model');
        if (true) {
            echo "{\"success\":true,pedido:" . $this->T_pedido_producto_tmp_model->hacer_pedido() . "}";
        } else {
            echo "{\"success\":false}";
        }
    }
    public function eliminar_pedido_tmp()
    {
        $this->load->model('T_pedido_producto_tmp_model');
        if ($this->T_pedido_producto_tmp_model->eliminar_pedido_tmp()) {
            echo "{\"success\":true}";
        } else {
            echo "{\"success\":false}";
        }
    }

    public function correo_pedido_estado($id_estado)
    {
        $this->load->model('Correo_model');
        $this->Correo_model->correo_pedido_estado($id_estado);
    }

    public function cambiar_estado_pedido()
    {
        $this->load->model('T_proforma_model');
        if ($this->T_proforma_model->cambiar_estado()) {
            echo "{\"success\":true}";
        } else {
            echo "{\"success\":false}";
        }
    }

    public function eliminar_producto_proforma()
    {
        $this->load->model('T_proforma_model');
        if ($this->T_proforma_model->eliminar_producto_proforma()) {
            echo "{success:true}";
        } else {
            echo "{\"success\":false}";
        }
    }

    public function insertar_subcategoria()
    {
        $this->load->model('N_subcategoria_model');
        if ($this->N_subcategoria_model->insertar_subcategoria()) {
            echo "{\"success\":true}";
        } else {
            echo "{\"success\":false}";
        }
    }

    public function insertar_producto_proforma()
    {
        $this->load->model('T_proforma_model');
        if ($this->T_proforma_model->insertar_producto_proforma()) {
            echo "{\"success\":true}";
        } else {
            echo "{\"success\":false}";
        }
    }

    public function buscar_productos()
    {
        $this->load->model('T_producto_model');
        echo "{\"data\":" . json_encode($this->T_producto_model->get_productos()) . "}";
    }

    public function buscar_usuarios()
    {
        $this->load->model('T_usuario_model');
        echo "{\"data\":" . json_encode($this->T_usuario_model->get_usuarios()) . "}";
    }

    public function cargar_usuario_por_id()
    {
        $this->load->model('T_usuario_model');
        echo "{success:true,\"data\":" . json_encode($this->T_usuario_model->get_usuario_by_id()) . "}";
    }

    public function modificar_producto()
    {
        $this->load->model('T_producto_model');
        echo "{success:true,\"data\":" . json_encode($this->T_producto_model->modificar_producto()) . "}";
    }

    public function eliminar_accion_por_id()
    {
        $this->load->model('T_accion_del_plan_model');
        if ($this->T_accion_del_plan_model->eliminar_accion()) {
            echo "{\"success\":true}";
        } else {
            echo "{\"success\":false}";
        }
    }

    public function eliminar_producto()
    {
        $this->load->model('T_producto_model');
        if ($this->T_producto_model->eliminar_producto()) {
            echo "{\"success\":true}";
        } else {
            echo "{\"success\":false}";
        }
    }

    public function cargar_producto_por_id()
    {
        $this->load->model('T_producto_model');
        echo "{success:true,data:" . json_encode($this->T_producto_model->get_producto_por_id()) . "}";
    }

    public function cargar_proforma_por_id()
    {
        $this->load->model('T_proforma_model');
        echo "{success:true,data:" . json_encode($this->T_proforma_model->get_proforma_por_id()) . "}";
    }

    function existe_empleado()
    {
        $this->load->model('T_empleados_model');
        $existe = $this->T_empleados_model->verificar_existe_empelado();
        if (!$existe) {
            echo "{\"success\":true,\"existe\":false}";
        } else {
            echo "{\"success\":true,\"existe\":true}";
        }
    }
    public function get_usuario_operador()
    {
        $this->load->model('T_usuario_model');
        echo "{\"data\":" . json_encode($this->T_usuario_model->get_usuario_operador()) . "}";
    }
    public function prueba($id_pedido)
    {
        $this->load->model('T_proforma_model');
        echo json_encode($this->T_proforma_model->get_proforma_excel($id_pedido));
    }
    public  function  exportar_excel_empleados($id_pedido=16)
    {
        $this->load->model('T_proforma_model');
       // echo json_encode($this->T_proforma_model->get_proforma_excel($id_pedido)['productos']);
        $this->load->library('PHPExcel');
        $this->load->library('PHPExcel/IOFactory');
        $proforma = $this->T_proforma_model->get_proforma_excel($id_pedido);
        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("title")
            ->setDescription("description");

        // Assign cell values


        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Ing. Ramon Luis Morales Vega")
            ->setLastModifiedBy("Ramon Luis")
            ->setTitle("Listado Empleados")
            ->setSubject("Listado Empleados")
            ->setDescription("Proforma")
            ->setKeywords("office 2007")
            ->setCategory("Categorias");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Proforma Pedido '.$proforma['proforma']->id_pedido);
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Descuento');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Precio');

        //$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Apellidos');
        $objPHPExcel->getActiveSheet()->mergeCells('A1:B1');
        $pos = 0;
        $count_productos = count($proforma['productos']);
        for ($i = 0; $i < $count_productos; $i++) {
            $pos = $i + 2;
            //echo 'A'.$pos;
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $pos, $proforma['productos'][$i]['marca']);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $pos, $proforma['productos'][$i]['modelo']);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $pos, $proforma['productos'][$i]['detalles']);
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $pos, $proforma['productos'][$i]['descuento_valor']);
            $objPHPExcel->getActiveSheet()->setCellValue('F' . $pos, $proforma['productos'][$i]['precio']);
        }
        $pos = $pos+2;
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $pos,'Iva '.$proforma['proforma']->iva );
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $pos, 'Subtotal '.$proforma['proforma']->subtotal);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $pos, 'Descuento '.$proforma['proforma']->descuento);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $pos, 'Total '.$proforma['proforma']->total);

        $styleArray = array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            )
        );

        $objPHPExcel->getActiveSheet()->getStyle('C'.$pos.':F1'.$pos)->applyFromArray($styleArray);
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Proformas');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client�s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="proforma.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }
}