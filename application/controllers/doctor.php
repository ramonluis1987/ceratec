<?php
ini_set('display_errors', 1);
if (!defined('BASEPATH'))
    exit ('No direct script access allowed');

class Doctor extends CI_Controller
{
    public function index()
    {

    }

    public function insertar_doctor()
    {
        $this->load->model('T_doctor_model');
        $ruc = $_POST['ruc'];
        if ($this->T_doctor_model->existe_ruc($ruc)) {
            echo "{\"existe\":true,\"success\":true}";
        } else {
            if ($this->T_doctor_model->insertar_doctor()) {
                echo "{\"existe\":false,\"success\":true}";
            }
        }
    }

    public function modificar_doctor()
    {
        $this->load->model('T_doctor_model');
        if ($this->T_doctor_model->modificar_doctor()) {
            echo "{\"success\":true}";
        }
        else{
            echo "{\"success\":false}";
        }

    }

    public function get_doctores()
    {
        $this->load->model('T_doctor_model');
        echo "{\"data\":" . json_encode($this->T_doctor_model->get_doctores()) . "}";
    }

    public function get_doctor_por_id()
    {
        $this->load->model('T_doctor_model');
        echo "{success:true,\"data\":" . json_encode($this->T_doctor_model->get_doctor_por_id()) . "}";
    }

    public function cargar_doctores()
    {
        $this->load->model('T_doctor_model');
        echo "{\"data\":" . json_encode($this->T_doctor_model->cargar_doctores()) . "}";
    }
}