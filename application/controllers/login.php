<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function index()
	{
		echo "{\"success\":true,\"autenticado\":false}";
	}
	public function autenticar()
	{
		
		$this->load->model('T_usuario_model');
		//$this->load->model('T_permiso_model');
		$result = $this->T_usuario_model->get_autenticacion();
		if(!empty($result))
		{
			$this->load->library('session');
			$newdata = array(
			'usuario'  => $result->usuario,
			'autenticado' => TRUE,
			//'id_rol'=> $result->id_rol,
			'nombre'=> $result->nombre." ".$result->apellidos,
			'id_usuario' => $result->id_usuario
			);
			$this->session->set_userdata($newdata);
			echo "{\"success\":true,\"autenticado\":true}";
		}
		else{
			echo "{\"success\":true,\"autenticado\":false}";
		}
		
	}
	public function cargar_usuario()
	{
		$this->load->library('session');
		$u = $this->session->userdata('autenticado');
		$this->load->database();;
		if($u)
		{
			echo json_encode($this->session->all_userdata());
		}
		else {
			echo "{\"success\":true,\"autenticado\":false}";
		}
	}
	public function cerrar_session()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
	}
}