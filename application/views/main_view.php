<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Registro de Trabajo</title>
    <link href="web/ext/resources/css/ext-all.css" rel="stylesheet" type="text/css"/>

    <link href="web/css/template.css" rel="stylesheet" type="text/css"/>
</head>
<script type="text/javascript">
    var base_url = '<?= base_url();?>';
    window.App = window.App || {};
    window.App.base_url = base_url;
</script>
<body>
<?= $this->load->view('librerias/main_js') ?>
<?= $this->load->view('comun/main_js') ?>
<?= $this->load->view('doctor/all_js') ?>
<?= $this->load->view('registro_trabajo/all_js') ?>
</body>

</html>